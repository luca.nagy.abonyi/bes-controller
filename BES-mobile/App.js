import React, {useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Home from './views/Home';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {NavigationContainer} from '@react-navigation/native';
import navigationRef from './navigationRef';
import Navbar from './views/Navbar';
import Settings from './views/Settings';
import AsyncStorage from '@react-native-async-storage/async-storage';
import AuthService from './services/authService';
import {Provider} from 'react-redux';
import store from './index';
import {auth, profile, pump} from './actions/types';
import Login from './views/Login';
import Middleware from './views/Middleware';

export default function App () {
  const Drawer = createDrawerNavigator ();

  async function load () {
    try {
      await AsyncStorage.getItem ('@manual')
        .then ()
        .catch (error => {
        });
        
      await AsyncStorage.getItem ('@pump_id');

    } catch (error) {
      console.log (error);
    }
  }

  async function auth (token) {
    await AuthService.auth (token).then (res => {
      if (res.data.message === 'Unauthorized.') {
        AsyncStorage.setItem ('@token', null);
      } else {
        store.dispatch (profile (res.data));
        store.dispatch (pump (res.data.pump_id));
      }
    });
  }

  load ();

  return (
    <Provider store={store}>
      <NavigationContainer style={styles.container} ref={navigationRef}>
        <Drawer.Navigator
          initialRouteName="Middleware"
          drawerContent={props => <Navbar {...props} />}
        >
          <Drawer.Screen
            name="Home"
            component={Home}
            options={{headerShown: false}}
          />
          <Drawer.Screen
            name="Settings"
            component={Settings}
            options={{headerShown: false}}
          />
          <Drawer.Screen
            name="Login"
            component={Login}
            options={{headerShown: false}}
          />
          <Drawer.Screen
            name="Middleware"
            component={Middleware}
            options={{headerShown: false}}
          />
        </Drawer.Navigator>
      </NavigationContainer>
    </Provider>
  );
}

const styles = StyleSheet.create ({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
