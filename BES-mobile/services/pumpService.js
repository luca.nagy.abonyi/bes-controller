import axios from 'axios';
import store from '../index';
import {setTemp} from '../actions/types';

class PumpService {
  async getPump (id, token) {
    try {
      return axios.get ('http://cloud.bes.rs:8000/api/pumps/' + id, {
        headers: {Authorization: `Bearer ${token}`},
      });
    } catch (error) {
      console.log (error);
    }
  }

  async update (id, pump) {
    try {
      return axios.put ('http://cloud.bes.rs:8000/api/pumps/' + id, pump, {
        headers: {Authorization: `Bearer ${store.getState ().token}`},
      });
    } catch (error) {
      console.log (error);
    }
  }

  async setTemp (id, temp) {
    let pump = {
      temp: temp
    };

    let updatePump = {
      set_temp: temp
    }

    try {
      this.update(id, updatePump)
      return axios.put (
        'http://cloud.bes.rs:8000/api/commands/setTemp/' + id,
        pump,
        {
          headers: {Authorization: `Bearer ${store.getState ().token}`},
        }
      );
    } catch (error) {
      console.log (error);
    }
  }

  async cool (id) {
    console.log (store.getState ().token);
    try {
      return axios.put ('http://cloud.bes.rs:8000/api/commands/cool/' + id, {
        headers: {Authorization: `Bearer ${store.getState ().token}`},
      });
    } catch (error) {
      console.log (error);
    }
  }

  async heat (id) {
    try {
      return axios.put ('http://cloud.bes.rs:8000/api/commands/heat/' + id, {
        headers: {Authorization: `Bearer ${store.getState ().token}`},
      });
    } catch (error) {
      console.log (error);
    }
  }

  async reset (id) {
    try {
      return axios.put ('http://cloud.bes.rs:8000/api/commands/reset/' + id, {
        headers: {Authorization: `Bearer ${store.getState ().token}`},
      });
    } catch (error) {
      console.log (error);
    }
    }

  async getCityTemp () {
    try {
      return axios.get (
        `http://api.openweathermap.org/data/2.5/weather?id=${store.getState().profile.address.city_code}&units=metric&lang=en&appid=b1ffa48ca579e88c8a13213bbe7cf9bd`
      );
    } catch (error) {

      console.log (error);
    }
  }

  async auto (pump, level, city_wheater, temp) {
    let set = this.getTemp (level, city_wheater, temp);
    this.setTemp (pump.id, set);
    store.dispatch (setTemp (set));

    pump.set_temp = set
      console.log(pump)

    try {
      return axios.put('http://cloud.bes.rs:8000/api/pumps/auto', pump, {
        headers: {Authorization: `Bearer ${store.getState ().token}`},
      });
    } catch (error) {
      console.log (error);
    }
  }

  async manual (pump) {
    try {
      return axios.put('http://cloud.bes.rs:8000/api/pumps/' + pump.id, pump, {
        headers: {Authorization: `Bearer ${store.getState ().token}`},
      });
    } catch (error) {
      console.log ('error');
    }
  }

  getTemp (level, city_wheater, temp) {
    if (level === 1) {
      if (city_wheater < 22 && city_wheater >= 15) {
        return 26;
      } else if (city_wheater < 15 && city_wheater >= 10) {
        return 27;
      } else if (city_wheater < 10 && city_wheater >= 5) {
        return 28;
      } else if (city_wheater < 5 && city_wheater >= 0) {
        return 29;
      } else if (city_wheater < 0 && city_wheater >= -5) {
        return 30;
      } else if (city_wheater < -5 && city_wheater >= -10) {
        return 31;
      } else if (city_wheater < -5 && city_wheater >= -10) {
        return 32;
      }
    } else if (level === 2) {
      if (city_wheater < 22 && city_wheater >= 15) {
        return 29;
      } else if (city_wheater < 15 && city_wheater >= 10) {
        return 30;
      } else if (city_wheater < 10 && city_wheater >= 5) {
        return 31;
      } else if (city_wheater < 5 && city_wheater >= 0) {
        return 32;
      } else if (city_wheater < 0 && city_wheater >= -5) {
        return 33;
      } else if (city_wheater < -5 && city_wheater >= -10) {
        return 34;
      } else if (city_wheater < -5 && city_wheater >= -10) {
        return 35;
      }
    } else if (level === 3) {
      if (city_wheater < 22 && city_wheater >= 15) {
        return 31;
      } else if (city_wheater < 15 && city_wheater >= 10) {
        return 32;
      } else if (city_wheater < 10 && city_wheater >= 5) {
        return 33;
      } else if (city_wheater < 5 && city_wheater >= 0) {
        return 34;
      } else if (city_wheater < 0 && city_wheater >= -5) {
        return 35;
      } else if (city_wheater < -5 && city_wheater >= -10) {
        return 36;
      } else if (city_wheater < -5 && city_wheater >= -10) {
        return 37;
      }
    } else if (level === 4) {
      if (city_wheater < 22 && city_wheater >= 15) {
        return 32;
      } else if (city_wheater < 15 && city_wheater >= 10) {
        return 34;
      } else if (city_wheater < 10 && city_wheater >= 5) {
        return 35.5;
      } else if (city_wheater < 5 && city_wheater >= 0) {
        return 38;
      } else if (city_wheater < 0 && city_wheater >= -5) {
        return 40;
      } else if (city_wheater < -5 && city_wheater >= -10) {
        return 42;
      } else if (city_wheater < -5 && city_wheater >= -10) {
        return 44;
      }
    } else if (level === 5) {
      if (city_wheater < 22 && city_wheater >= 15) {
        return 34;
      } else if (city_wheater < 15 && city_wheater >= 10) {
        return 36;
      } else if (city_wheater < 10 && city_wheater >= 5) {
        return 37;
      } else if (city_wheater < 5 && city_wheater >= 0) {
        return 40;
      } else if (city_wheater < 0 && city_wheater >= -5) {
        return 42;
      } else if (city_wheater < -5 && city_wheater >= -10) {
        return 44;
      } else if (city_wheater < -5 && city_wheater >= -10) {
        return 46;
      }
    } else if (level === 6) {
      if (city_wheater < 22 && city_wheater >= 15) {
        return 36;
      } else if (city_wheater < 15 && city_wheater >= 10) {
        return 37;
      } else if (city_wheater < 10 && city_wheater >= 5) {
        return 38;
      } else if (city_wheater < 5 && city_wheater >= 0) {
        return 42;
      } else if (city_wheater < 0 && city_wheater >= -5) {
        return 44;
      } else if (city_wheater < -5 && city_wheater >= -10) {
        return 46;
      } else if (city_wheater < -5 && city_wheater >= -10) {
        return 48;
      }
    } else {
      if (city_wheater < 22 && city_wheater >= 15) {
        return 38;
      } else if (city_wheater < 15 && city_wheater >= 10) {
        return 39;
      } else if (city_wheater < 10 && city_wheater >= 5) {
        return 40;
      } else if (city_wheater < 5 && city_wheater >= 0) {
        return 42;
      } else if (city_wheater < 0 && city_wheater >= -5) {
        return 44;
      } else if (city_wheater < -5 && city_wheater >= -10) {
        return 46;
      } else if (city_wheater < -5 && city_wheater >= -10) {
        return 50;
      }
    }
    return 0;
  }
}

export default new PumpService ();
