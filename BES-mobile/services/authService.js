import axios from 'axios';
import store from '../index';

class AuthService {
  async auth (token) {
    try {
      return axios.get ('http://cloud.bes.rs:8000/api/profile', 
      {
        headers: {Authorization: `Bearer ${token}`},
      });
    } catch (error) {
      console.log (error);
    }
  }

  async login (json) {
    try {
      return axios.post ('http://cloud.bes.rs:8000/api/login', json);
    } catch (error) {
      console.log (error);
    }
  }

}

export default new AuthService ();
