import {AUTH, SET_TEMP, PROFILE, PUMP} from './actions';

export function auth (payload) {
  return {type: AUTH, payload};
}

export function setTemp (payload) {
  return {type: SET_TEMP, payload};
}

export function profile (payload) {
  return {type: PROFILE, payload};
}

export function pump (payload) {
  return {type: PUMP, payload};
}