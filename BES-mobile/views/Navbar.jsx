import React, { Component } from 'react'
import { StyleSheet, Text, View, TouchableOpacity, Image } from 'react-native';
import { Icon } from 'react-native-elements';
import { Dimensions } from 'react-native';
import {
    DrawerItem
} from '@react-navigation/drawer';

class NavBar extends Component {
    constructor(props) {
        super(props)

    }

    componentDidMount() {

    }

    openMenu() {

    }

    render() {
        return (
            <View style={navbar.menuContainer}>
                <View style={navbar.menuHeader}>
                    <Image style={navbar.logo} source={require('../assets/logo2.png')} />
                </View>
                <View style={navbar.menuItems}>
                    <DrawerItem
                        icon={({ color, size }) => (
                            <Icon
                                name='home'
                                type='ionicon'
                                size={30}
                                color='#000000'
                                style={{ marginLeft: 10 }}
                            />
                        )}
                        label={() => <Text style={navbar.menuItemText}>Home</Text>}
                        onPress={() => { this.props.navigation.navigate('Home') }}>
                    </DrawerItem>
                    <DrawerItem
                        icon={({ color, size }) => (
                            <Icon
                                name='settings'
                                type='ionicon'
                                size={30}
                                color='#000000'
                                style={{ marginLeft: 10 }}
                            />
                        )}
                        label={() => <Text style={navbar.menuItemText}>Settings</Text>}
                        onPress={() => { this.props.navigation.navigate('Settings') }}>
                    </DrawerItem>
                </View>
            </View>
        )
    }
}
const width = Dimensions.get('window').width;
const height = Dimensions.get("window").height;

const navbar = StyleSheet.create({
    container: {
        position: 'absolute',
        top: 0,
        width: width
    },
    navbarContainer: {
        backgroundColor: '#ffffff',
        height: 100,
        display: 'flex',
        justifyContent: 'space-between',
        padding: 10,
        alignItems: 'center',
        flexDirection: 'row',
        zIndex: 1,
        width: width,
        paddingTop: 60
    },
    h1: {
        color: '#000000',
        fontSize: 18,
        letterSpacing: 1
    },
    menuContainer: {
        height: '100%',
        backgroundColor: '#ffffff',
        color: '#000000',
        fontSize: 15,
        zIndex: 10,
        position: 'absolute',
        textAlign: 'left',
        display: 'flex',
        paddingTop: 30,
        width: '100%',

    },
    menuHeader: {
        display: 'flex',
        justifyContent: 'space-around',
        paddingTop: 8,
        paddingBottom: 10,
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 30,
    },
    menuTitle: {
        color: '#000000',
        fontSize: 20,
        letterSpacing: 1,
    },
    menuItem: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',

    },
    menuItemText: {
        color: '#000000',
        fontSize: 16,
        letterSpacing: 1,
        marginBottom: 10,
        marginTop: 10,
        width: 130,
        textTransform: 'uppercase'
    },
    langContainer: {
        display: 'flex',
        flexDirection: 'row',
        width: '100%',
        marginLeft: 30,
        marginTop: 20
    },
    lang: {
        marginRight: 30
    },
    logo: {
        position: 'relative',
        width: 60,
        height: 50,
    }
})

export default NavBar