import React, { Component } from 'react'
import { StyleSheet, Text, Image, TouchableOpacity, View, Modal, Pressable } from 'react-native';
import { Icon } from 'react-native-elements';
import { Dimensions } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import PumpService from '../services/pumpService';
import store from '../index';
import authService from '../services/authService';
import { useNavigation } from '@react-navigation/native';
import { setTemp, auth, profile } from '../actions/types';
import RNRestart from 'react-native-restart';

export default class Settings extends Component {
    constructor(props) {
        super(props)
        this.state = {
            modal: false,
            cityWeather: 0,
            manual: false,
            pump: {},
        }
    }

    componentDidMount() {
        this.getData()
    }

    async getData() {
        await PumpService.getCityTemp().then(res => {
            this.setState({
                cityWeather: res.data.main.temp,
            })
        })
        await PumpService.getPump(store.getState().pump, store.getState().token).then(res => {
            this.setState({
                pump: res.data,
                manual: res.data.auto > 0 ? true : false
            })

        })
    }

    async setAutoLevel(value) {
        
        let pump = {
            id: this.state.pump.id,
            auto: value,
        }

        await PumpService.auto(pump, value, this.state.cityWeather, this.state.pump.temperature).then(res => {
            this.getData()
            this.setState({
                modal: false,
                manual: value > 0 ? true : false

            })
        })
    }

    setModalVisible(value) {
        this.setState({
            modal: value
        })
    }

    async logout() {
        await AsyncStorage.removeItem('@token').then(() => {
            store.dispatch(auth(null))
            store.dispatch(profile({}))
            this.props.navigation.navigate('Login')

        });

    }

    render() {
        return (
            <View style={styles.container} >
                <View style={styles.topbar}>
                    <View style={styles.menu}>
                        <OpenMenuButton />
                    </View>
                    <Text style={styles.topbarTitle}>SETTINGS</Text>
                    <View style={{ marginRight: 50 }}></View>
                </View>

                <View style={styles.row}>
                    <Text style={styles.title}>Pump settings</Text>
                    <TouchableOpacity
                        style={this.state.manual ? styles.activeButton : styles.actionButton}
                        onPress={() => this.setModalVisible(true)}
                    >
                        <Text>AUTO</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => this.setAutoLevel(0)}
                        style={!this.state.manual ? styles.activeButton : styles.actionButton}
                    >
                        <Text>MANUAL</Text>
                    </TouchableOpacity>
                </View>

                <View style={styles.row}>
                    <Text style={styles.title}>Profile settings</Text>
                    <TouchableOpacity
                        onPress={() => this.logout()}
                    >
                        <Text style={styles.log}>Log out</Text>
                    </TouchableOpacity>
                </View>


                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.modal}
                    onRequestClose={() => {
                        this.setModalVisible(!this.state.modal);
                    }}
                >
                    <View style={styles.centeredView}>
                        <View style={styles.modalView}>
                            <Pressable
                                style={{
                                    borderRadius: 10,
                                    padding: 10,
                                    elevation: 2,
                                    minWidth: 120,
                                    marginBottom: 10,
                                    backgroundColor: '#FF8236'
                                }}
                                onPress={() => this.setAutoLevel(1)}
                            >
                                <Text style={styles.textStyle}>1</Text>
                            </Pressable>
                            <Pressable
                                style={{
                                    borderRadius: 10,
                                    padding: 10,
                                    elevation: 2,
                                    minWidth: 120,
                                    marginBottom: 10,
                                    backgroundColor: '#FF7B42'
                                }}
                                onPress={() => this.setAutoLevel(2)}
                            >
                                <Text style={styles.textStyle}>2</Text>
                            </Pressable>
                            <Pressable
                                style={{
                                    borderRadius: 10,
                                    padding: 10,
                                    elevation: 2,
                                    minWidth: 120,
                                    marginBottom: 10,
                                    backgroundColor: '#FF7250'
                                }}
                                onPress={() => this.setAutoLevel(3)}
                            >
                                <Text style={styles.textStyle}>3</Text>
                            </Pressable>
                            <Pressable
                                style={{
                                    borderRadius: 10,
                                    padding: 10,
                                    elevation: 2,
                                    minWidth: 120,
                                    marginBottom: 10,
                                    backgroundColor: '#FF5766'
                                }}
                                onPress={() => this.setAutoLevel(4)}
                            >
                                <Text style={styles.textStyle}>4</Text>
                            </Pressable>
                            <Pressable
                                style={{
                                    borderRadius: 10,
                                    padding: 10,
                                    elevation: 2,
                                    minWidth: 120,
                                    marginBottom: 10,
                                    backgroundColor: '#FE4454'
                                }}
                                onPress={() => this.setAutoLevel(5)}
                            >
                                <Text style={styles.textStyle}>5</Text>
                            </Pressable>
                            <Pressable
                                style={{
                                    borderRadius: 10,
                                    padding: 10,
                                    elevation: 2,
                                    minWidth: 120,
                                    marginBottom: 10,
                                    backgroundColor: '#FE3849'
                                }}
                                onPress={() => this.setAutoLevel(6)}
                            >
                                <Text style={styles.textStyle}>6</Text>
                            </Pressable>
                            <Pressable
                                style={{
                                    borderRadius: 10,
                                    padding: 10,
                                    elevation: 2,
                                    minWidth: 120,
                                    marginBottom: 10,
                                    backgroundColor: '#FD2C3E'
                                }}
                                onPress={() => this.setAutoLevel(7)}
                            >
                                <Text style={styles.textStyle}>7</Text>
                            </Pressable>

                            <Pressable
                                style={[styles.button, styles.buttonClose]}
                                onPress={() => this.setModalVisible(!this.state.modal)}
                            >
                                <Text style={styles.textStyle}>CANCEL</Text>
                            </Pressable>
                        </View>
                    </View>
                </Modal>
            </View>
        )
    }
}


function OpenMenuButton() {
    const navigation = useNavigation();
    return (<Icon
        name='menu-outline'
        type='ionicon'
        color='#ffffff'
        size={40}
        onPress={() => {
            navigation.openDrawer()
        }}
    />)
}

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        /*  alignItems: 'center',
         justifyContent: 'center',
  */
    },
    topbar: {
        height: 80,
        width: width,
        marginBottom: 40,
        backgroundColor: '#FF8235',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        width: width
    },
    topbarTitle: {
        color: '#ffffff',
        fontWeight: '700',
        fontSize: 20,
        letterSpacing: 2,
        marginTop: 10
    },

    menu: {
        marginLeft: 10,
        marginTop: 10

    },
    row: {
        marginHorizontal: 20,
        marginBottom: 20
    },
    title: {
        color: '#000000',
        marginBottom: 20,
        borderBottomColor: '#f6f6f6',
        borderBottomWidth: 1,
    },
    actionButton: {
        shadowColor: "#000",
        shadowOffset: {
            width: 5,
            height: 5,
        },
        shadowOpacity: 0.15,
        shadowRadius: 15,
        elevation: 1,
        width: 100,
        height: 50,
        borderRadius: 15,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 20,
        backgroundColor: '#ffffff',
        textTransform: 'uppercase',
        fontWeight: '500',
        minWidth: 150,
        margin: 10
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
    },
    log: {
        textDecorationLine: 'underline',
        textDecorationColor: '#000',
        textDecorationStyle: 'solid',
    },
    button: {
        borderRadius: 10,
        padding: 10,
        elevation: 2,
        minWidth: 120,
        marginBottom: 10,
        backgroundColor: '#ffffff',
    },
    buttonOpen: {
        backgroundColor: "#F194FF",
    },
    buttonClose: {
        backgroundColor: "#ffffff",
    },
    textStyle: {
        color: "black",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center"
    },
    activeButton: {
        shadowColor: "#000",
        shadowOffset: {
            width: 5,
            height: 5,
        },
        shadowOpacity: 0.15,
        shadowRadius: 15,
        elevation: 1,
        width: 100,
        height: 50,
        borderRadius: 15,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 20,
        textTransform: 'uppercase',
        fontWeight: '500',
        minWidth: 150,
        margin: 10,
        backgroundColor: '#FF5F6D',
    }
})