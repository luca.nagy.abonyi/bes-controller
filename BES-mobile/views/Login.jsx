import React, { Component } from 'react'
import { StyleSheet, Text, Image, TextInput, View, SafeAreaView, Button } from 'react-native';
import { Dimensions } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import PumpService from '../services/pumpService';
import { LinearGradient } from 'expo-linear-gradient';
import AuthService from '../services/authService';
import { auth, profile, pump } from '../actions/types'
import store from '../index';

export default class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            phone: null,
            password: null,
            /* phone: '0631172576',
            password: '123456789',*/
        }
    }

    componentDidMount() {
        this.getData()
    }

    async getData() {
        /* await PumpService.getCityTemp().then(res => {
            this.setState({
                cityWeather: res.data.main.temp,
            })
        }) */
    }

    onPhoneChange(text) {
        this.setState({
            phone: text
        })
    }

    onPassChange(text) {
        this.setState({
            password: text
        })
    }

    focus() {
        this.setState({
            phone: ''
        })
    }

    focusPass() {
        this.setState({
            password: ''
        })
    }

    async login() {
        await AuthService.login(this.state).then(res => {
            store.dispatch(auth(res.data.token))

            AsyncStorage.setItem('@token', res.data.token)
            this.auth(res.data.token)
        }).catch(error => {
            if (error.response.status === 422) {
                this.setState({
                    phone_error: error.response.data.phone,
                    password_error: error.response.data.password,
                    error: undefined

                })
            } else {
                this.setState({
                    phone_error: undefined,
                    password_error: undefined,
                    error: "The phone number or the password is invalid.",
                })
            }

        })
    }

    async auth(token) {
        await this.getProfile(token)
    }

    async getProfile(token) {
        await AuthService.auth(token).then(res => {
            if (res.data.message === 'Unauthorized.') {
                AsyncStorage.setItem('@token', null);
            } else {
                store.dispatch(profile(res.data))
                store.dispatch(pump(res.data.pump_id));
                this.props.navigation.navigate('Home')
            }
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <LinearGradient
                    colors={['#FF8235', '#FF5F6D']}
                    style={styles.background}>
                    <View styles={styles.box}>
                        <View style={styles.center}>
                            <View style={styles.centerItems}>
                                <Image style={styles.logo} source={require('../assets/logo2.png')} />
                                <Text style={styles.text}>Heat Pump Controller</Text>
                            </View>

                            <TextInput
                                style={styles.input}
                                onChangeText={text => this.onPhoneChange(text)}
                                onFocus={() => this.focus()}
                                value={this.state.phone}
                                keyboardType="numeric"
                                autoCompleteType="tel"
                                defaultValue="Phone number"
                            />
                            {
                                this.state.phone_error === undefined ?
                                    <View></View>
                                    :
                                    <Text style={styles.error}>
                                        {this.state.phone_error}
                                    </Text>
                            }
                            <TextInput
                                style={styles.input}
                                onChangeText={text => this.onPassChange(text)}
                                onFocus={() => this.focusPass()}
                                value={this.state.password}
                                defaultValue="Password"
                                secureTextEntry={true}
                            />
                            {
                                this.state.password_error === undefined ?
                                    <View></View>
                                    :
                                    <Text style={styles.error}>
                                        {this.state.password_error}
                                    </Text>
                            }
                            {
                                this.state.error === undefined ?
                                    <View></View>
                                    :
                                    <Text style={styles.error}>
                                        {this.state.error}
                                    </Text>
                            }
                            <Button
                                title="LOGIN"
                                onPress={() => this.login()}
                                buttonStyle={styles.button}
                                color='#000'
                            />
                        </View>

                    </View>
                </LinearGradient>

            </View>
        )

    }
}


const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    background: {
        alignItems: 'center',
        justifyContent: 'center',
        width: width,
        height: height,
        position: 'absolute',
    },
    box: {
        padding: 20,
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        textAlign: 'center',
        position: 'absolute',
        zIndex: 100,
        top: 10,
        width: 300,
        height: 140,
        backgroundColor: '#ffffff',
    },
    center: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        textAlign: 'center',
        backgroundColor: '#ffffff',
        shadowColor: "#000",
        shadowOffset: {
            width: -0,
            height: 5,
        },
        shadowOpacity: 0.15,
        shadowRadius: 15,
        elevation: 1,
        borderRadius: 20,
        padding: 20

    },
    centerItems: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
    },
    logo: {
        //position: 'relative',
        width: 80,
        height: 70,
    },
    text: {
        fontSize: 16,
        marginBottom: 10,
        fontWeight: '700',
        marginBottom: 20
    },
    input: {
        height: 40,
        marginBottom: 20,
        borderWidth: 1,
        padding: 10,
        width: 200,
    },
    button: {
        color: '#000000',
        maxWidth: 100,
        marginTop: 20
    },
    error: {
        color: 'red',
        marginTop: -20,
        marginBottom: 10,
        width: 200
    }
})
