import React, { Component } from 'react'
import { StyleSheet, Text, Image, TextInput, View, ActivityIndicator, Button } from 'react-native';
import { Dimensions } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import AuthService from '../services/authService';
import { auth, profile, pump } from '../actions/types'
import store from '../index';
import { LinearGradient } from 'expo-linear-gradient';

export default class Middleware extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }
    }

    componentDidMount() {
        this.getData()
    }

    async getData() {
        await AsyncStorage.getItem('@token').then(res => {
            if (res) {
                this.auth(res)
            } else {
                this.props.navigation.navigate('Login')
            }
        }
        );
    }

    auth(token) {
        this.getProfile(token)
    }

    async getProfile(token) {
        await AuthService.auth(token).then(res => {
            if (!res.data) {
                AsyncStorage.setItem('@token', null);
                this.props.navigation.navigate('Login')
            } else {
                store.dispatch(profile(res.data))
                store.dispatch(pump(res.data.pump_id));
                this.props.navigation.navigate('Home')
            }
        }).catch(error => {
            this.props.navigation.navigate('Login')
        })
    }

    render() {
        return (
            <View>
                <LinearGradient
                    colors={['#FF8235', '#FF5F6D']}
                    style={styles.background}>
                    <View styles={styles.box}>
                        <ActivityIndicator size={100} color="#ffffff" />
                    </View>
                </LinearGradient>

            </View>
        )
    }
}

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    background: {
        alignItems: 'center',
        justifyContent: 'center',
        width: width,
        height: height,
        position: 'absolute',
    },
    box: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        textAlign: 'center',
        alignItems: 'center',
        position: 'absolute',
        width: 300,
        top: 200,
        height: height
    },

})