import React, { Component } from 'react'
import { StyleSheet, Text, Image, TouchableOpacity, View, Alert } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { Dimensions } from 'react-native';
import { Icon } from 'react-native-elements';
import PumpService from '../services/pumpService';
import AsyncStorage from '@react-native-async-storage/async-storage';
import store from '../index';
import { useNavigation } from '@react-navigation/native';
import { Popable } from 'react-native-popable';
import { setTemp, auth, profile } from '../actions/types';
import { Slider } from 'react-native-elements';
import AuthService from '../services/authService';

export default class Home extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: {},
            pump_temp: 0,
            prev_temp: 0,
            pump: {},
            cityWeather: 0,
            pump_temp_conf: 0,
            cityName: '',
            manual: 'false',
            token: store.getState().token,
            setTemp: '-',
            lastButton: 0,
            pump_id: null,
            loaded: false,
            change: false,
            set_temp: 0
        }
    }

    componentDidMount() {
        this.getData()
        this.getWeather()

        this.interval = setInterval(() => {
            this.getData()
        }, 6000)

        this.secInterval = setInterval(() => {
            this.getWeather()
        }, 60000)

    }

    componentDidUpdate() {
        if (!this.state.loaded) {
            this.getData()
        }
    }

    componentWillUnmount() {
        clearInterval(this.interval)
    }

    async getData() {
        let value = await AsyncStorage.getItem('@manual')

        if (Number((this.state.setTemp)).toFixed(1) === Number((this.state.temperature)).toFixed(1)) {
            this.setState({
                lastButton: 0
            })
        }
        if (!this.state.change) {
            await PumpService.getPump(store.getState().profile.pump_id, this.state.token).then(res => {
                this.setState({
                    data: res.data,
                    pump_temp: res.data.temperature,
                    prev_temp: res.data.previuos_temperature,
                    pump_temp_conf: res.data.temperature,
                    manual: 'true',
                    token: store.getState().token,
                    setTemp: res.data.set_temp,
                    pump: res.data,
                    pump_id: store.getState().profile.pump_id,
                    loaded: true,
                    set_temp: res.data.temperature
                })
            }).catch(error => {
            })
        }

    }
    //ez OK
    async getWeather() {
        await PumpService.getCityTemp().then(res => {
            this.setState({
                cityWeather: res.data.main.temp,
                cityName: res.data.name
            })
        })
    }

    plus() {
        let pump = Number(this.state.pump_temp)

        pump += 0.1
        this.setState({
            pump_temp: pump,
            set_temp: pump,
            lastButton: 1,
            change: true
        })
    }

    minus() {
        let pump = Number(this.state.pump_temp)

        pump -= 0.1
        this.setState({
            pump_temp: pump,
            set_temp: pump,
            lastButton: 2,
            change: true
        })
    }

    sliderTemp(value) {

    }

    async setTemp() {
        let pump = this.state.data
        pump.temp = this.state.pump_temp

        store.dispatch(setTemp(this.state.pump_temp))

        this.setState({
            pump_temp_conf: this.state.pump_temp,
            pump_temp: this.state.data.temperature,
            setTemp: store.getState().setTemp,
            change: false
        })

        await PumpService.setTemp(this.state.pump_id, this.state.pump_temp).then(res => {
            this.getData()
        })
    }

    async heat() {
        await PumpService.heat(this.state.pump_id).then(res => {
            this.setState({
                lastButton: 3,
                change: true
            })
        })
    }

    async cool() {
        await PumpService.cool(this.state.pump_id).then(res => {
            this.setState({
                lastButton: 4,
            })
        })
    }

    cancel() {
        this.setState({
            pump_temp: this.state.prev_temp,
            lastButton: 0,
            change: false,
            set_temp: this.state.prev_temp
        })
    }

    slideTemp(value) {
        this.setState({
            pump_temp: value,
            set_temp: value,
            change: true
        })
    }

    render() {
        if (this.state.state !== false) {
            return (
                <View style={styles.container}>
                    <LinearGradient
                        colors={['#FF8235', '#FF5F6D']}
                        style={styles.header}>
                        <View style={styles.navbar}>
                            <View style={styles.menu}>
                                <OpenMenuButton />
                            </View>
                            <Image style={styles.logo} source={require('../assets/logo2.png')} />
                            <View style={{ marginRight: 50 }}></View>
                        </View>
                    </LinearGradient >

                    <View style={styles.data}>
                        <View style={styles.dataRow}>
                            <View style={styles.dataItem}>
                                <Text style={styles.dataText}>{Number((this.state.cityWeather)).toFixed(1)} &#186;C</Text>
                                <Text style={styles.dataSubText}>wheater</Text>

                            </View>
                            <View style={styles.dataItem}>
                                <Text style={styles.dataText}>{Number((this.state.pump.set_temp)).toFixed(1)} &#186;C</Text>

                                <Text style={styles.dataSubText}>set</Text>
                            </View>
                        </View>
                        <View style={styles.dataRow}>
                            <View style={styles.dataItem}>
                                <Text style={styles.dataText}>{this.state.cityName}</Text>
                                <Text style={styles.dataSubText}>city</Text>
                            </View>
                            <View style={styles.dataItem}>
                                <Text style={styles.dataText}>{Number((this.state.data.temperature)).toFixed(1)} &#186;C</Text>
                                <Text style={styles.dataSubText}>actual</Text>
                            </View>
                        </View>
                    </View>

                    <View style={styles.content}>
                        {
                            this.state.change ?
                                <View>
                                    <Text style={styles.temperature}>{Number((this.state.set_temp)).toFixed(1)} &#186;C</Text>
                                </View>
                                :
                                <View>
                                    <Text style={styles.temperature}>{Number((this.state.pump_temp)).toFixed(1)} &#186;C</Text>
                                </View>
                        }


                    </View>
                    <View style={{ flex: 1, alignItems: 'stretch', justifyContent: 'center', width: width - 100, }}>
                        <Slider
                            value={this.state.set_temp}
                            onValueChange={(value) => this.slideTemp(value)}
                            thumbStyle={{ height: 20, width: 20, backgroundColor: '#FF5F6D' }}
                            step={0.1}
                            maximumValue={50}
                            minimumValue={12}
                            disabled={this.state.pump.auto > 0 ? true : false}
                        />
                    </View>
                    <View>
                        {
                            this.state.data.online === 0 || this.state.pump.auto > 0 ?
                                <View style={styles.buttons}>
                                    <TouchableOpacity style={styles.button}>
                                        <Icon
                                            name='add'
                                            type='ionicon'
                                            color='#a1a1a1'
                                            size={40}
                                        />
                                    </TouchableOpacity >
                                    <TouchableOpacity style={styles.button}>
                                        <Icon
                                            name='remove'
                                            type='ionicon'
                                            color='#a1a1a1'
                                            size={40}

                                        />
                                    </TouchableOpacity >
                                    <TouchableOpacity style={styles.button}>
                                        <Icon
                                            name='flame'
                                            type='ionicon'
                                            color='#a1a1a1'
                                            size={40}
                                        />
                                    </TouchableOpacity >
                                    <TouchableOpacity style={styles.button}>
                                        <Icon
                                            name='snowflake-o'
                                            type='font-awesome'
                                            color='#a1a1a1'
                                            size={40}
                                        />
                                    </TouchableOpacity >
                                </View>
                                :
                                <View style={styles.buttons}>
                                    {
                                        this.state.lastButton === 1 ?
                                            <TouchableOpacity style={styles.lastButton} onPress={() => {
                                                this.plus()
                                            }}>
                                                <Icon
                                                    name='add'
                                                    type='ionicon'
                                                    color='#ffffff'
                                                    size={40}
                                                />
                                            </TouchableOpacity >
                                            :
                                            <TouchableOpacity style={styles.button} onPress={() => {
                                                this.plus()
                                            }}>
                                                <Icon
                                                    name='add'
                                                    type='ionicon'
                                                    color='#FF5F6D'
                                                    size={40}
                                                />
                                            </TouchableOpacity >

                                    }
                                    {
                                        this.state.lastButton === 2 ?
                                            <TouchableOpacity style={styles.lastButton} onPress={() => {
                                                this.minus()
                                            }}>
                                                <Icon
                                                    name='remove'
                                                    type='ionicon'
                                                    color='#ffffff'
                                                    size={40}

                                                />
                                            </TouchableOpacity >
                                            :
                                            <TouchableOpacity style={styles.button} onPress={() => {
                                                this.minus()
                                            }}>
                                                <Icon
                                                    name='remove'
                                                    type='ionicon'
                                                    color='#FF5F6D'
                                                    size={40}

                                                />
                                            </TouchableOpacity >
                                    }
                                    {
                                        this.state.lastButton === 3 ?
                                            <TouchableOpacity style={styles.lastButton} onPress={() => this.heat()}>
                                                <Icon
                                                    name='flame'
                                                    type='ionicon'
                                                    color='#ffffff'
                                                    size={40}
                                                />
                                            </TouchableOpacity >
                                            :

                                            <TouchableOpacity style={styles.button} onPress={() => this.heat()}>
                                                <Icon
                                                    name='flame'
                                                    type='ionicon'
                                                    color='#FF5F6D'
                                                    size={40}
                                                />
                                            </TouchableOpacity >
                                    }
                                    {
                                        this.state.lastButton === 4 ?
                                            <TouchableOpacity style={styles.lastButton} onPress={() => this.cool()}>
                                                <Icon
                                                    name='snowflake-o'
                                                    type='font-awesome'
                                                    color='#ffffff'
                                                    size={40}
                                                />
                                            </TouchableOpacity >
                                            :
                                            <TouchableOpacity style={styles.button} onPress={() => this.cool()}>
                                                <Icon
                                                    name='snowflake-o'
                                                    type='font-awesome'
                                                    color='#FF5F6D'
                                                    size={40}
                                                />
                                            </TouchableOpacity >
                                    }
                                </View>
                        }
                    </View>
                    <View style={styles.actionButtons}>
                        <TouchableOpacity
                            style={styles.actionButton}
                            onPress={() => this.setTemp()}
                        >
                            <Text>Set</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={styles.actionButton}
                            onPress={() => this.cancel()}
                        >
                            <Text>Cancel</Text>
                        </TouchableOpacity>

                    </View>
                    <View style={{
                        display: 'flex',
                        flexDirection: 'row',
                        width: width - 100,
                        alignItems: 'center',
                        justifyContent: 'space-between',
                        marginHorizontal: 50,
                        marginBottom: 30
                    }}>
                        {
                            this.state.data.online === 0 ?
                                <Popable content="Offline">
                                    <Icon
                                        name='cloud-offline-outline'
                                        type='ionicon'
                                        size={30}
                                        color='#000'
                                        style={{ marginLeft: 10 }}
                                    />
                                </Popable>
                                :

                                <Popable content="Online">
                                    <Icon
                                        name='cloud-outline'
                                        type='ionicon'
                                        size={30}
                                        color='#000'
                                        style={{ marginLeft: 10 }}
                                    />
                                </Popable>
                        }

                        {
                            this.state.pump.auto > 0 ?
                                <Popable content={"AUTO level: " + this.state.pump.auto}>
                                    <Icon
                                        name='airplane'
                                        type='ionicon'
                                        size={30}
                                        color='#000'
                                        style={{ marginLeft: 40 }}
                                    />
                                </Popable>
                                :
                                <Popable content="MANUAL">
                                    <Icon
                                        name='hand-left'
                                        type='ionicon'
                                        size={30}
                                        color='#000'
                                        style={{ marginLeft: 40 }}
                                    />
                                </Popable>
                        }

                    </View>

                </View >

            )
        } else {
            return (
                <View>
                    <Text>HALO</Text>
                </View>
            )
        }
    }
}

function OpenMenuButton() {
    const navigation = useNavigation();
    return (<Icon
        name='menu-outline'
        type='ionicon'
        color='#ffffff'
        size={40}
        onPress={() => {
            navigation.openDrawer()
        }}
    />)
}

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',

    },
    header: {
        height: 190,
        position: 'absolute',
        top: -10,
        width: width,
        borderBottomEndRadius: 60,
        borderBottomLeftRadius: 60,
        borderBottomRightRadius: 60
    },
    navbar: {
        display: "flex",
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 40,
    },
    menu: {
        marginLeft: 10,
    },
    logo: {
        position: 'relative',
        width: 60,
        height: 50,
    },
    data: {
        width: 300,
        height: 140,
        backgroundColor: '#ffffff',
        shadowColor: "#000",
        shadowOffset: {
            width: -0,
            height: 5,
        },
        shadowOpacity: 0.15,
        shadowRadius: 15,
        elevation: 1,
        borderRadius: 20,
        position: 'absolute',
        top: 80,
        padding: 20
    },
    dataRow: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginBottom: 20
    },
    dataItem: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    dataText: {
        fontWeight: '600'
    },
    dataSubText: {
        fontWeight: '300',
        color: '#898787'
    },
    content: {
        marginTop: 250,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        maxHeight: 100

    },
    thermo: {
        marginLeft: -30
    },
    thermoPic: {
        width: 88,
        height: 264,
    },
    temperature: {
        fontSize: 56,
        fontWeight: '100',
        width: width,
        textAlign: 'center'
    },
    buttons: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        marginHorizontal: 10,
        bottom: 50,
        marginTop: 60
    },
    button: {
        shadowColor: "#000",
        shadowOffset: {
            width: 5,
            height: 5,
        },
        shadowOpacity: 0.15,
        shadowRadius: 15,
        elevation: 1,
        width: 60,
        height: 60,
        borderRadius: 15,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 20,
        backgroundColor: '#ffffff',
    },
    lastButton: {
        shadowColor: "#000",
        shadowOffset: {
            width: 5,
            height: 5,
        },
        shadowOpacity: 0.15,
        shadowRadius: 15,
        elevation: 1,
        width: 60,
        height: 60,
        borderRadius: 15,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 20,
        backgroundColor: '#FF5F6D',
    },
    actionButtons: {
        display: 'flex',
        flexDirection: 'row',
        marginBottom: 50
    },
    actionButton: {
        shadowColor: "#000",
        shadowOffset: {
            width: 5,
            height: 5,
        },
        shadowOpacity: 0.15,
        shadowRadius: 15,
        elevation: 1,
        width: 100,
        height: 50,
        borderRadius: 15,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 20,
        backgroundColor: '#ffffff',
        textTransform: 'uppercase',
        fontWeight: '500'
    },
    slider: {
        flex: 1,
        marginLeft: 10,
        marginRight: 10,
        alignItems: "stretch",
        justifyContent: "center"
    }
});

