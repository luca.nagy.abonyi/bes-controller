import {AUTH, SET_TEMP, PROFILE, PUMP} from '../actions/actions';

const initialState = {
  token: '',
  setTemp: '-',
  profile: {},
  pump: ''
};

function rootReducer (state = initialState, action) {
  if (action.type === AUTH) {
    return Object.assign ({}, state, {
      token: action.payload,
    });
  }

  if (action.type === SET_TEMP) {
    return Object.assign ({}, state, {
      setTemp: action.payload,
    });
  }

  if (action.type === PROFILE) {
    return Object.assign ({}, state, {
      profile: action.payload,
    });
  }

  if (action.type === PUMP) {
    return Object.assign ({}, state, {
      pump: action.payload,
    });
  }
}

export default rootReducer;
