<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pump;
use App\Address;
use App\Command;
use Illuminate\Support\Facades\Auth;
use App\User;

class ChartController extends Controller
{
    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getCitiesTemp()
    {
        $Addresses = Address::all();
        $citycodes = array();

        for ($i = 0; $i < count($Addresses); $i++) {
            $row = (object)[
                'name' => $Addresses[$i]->city,
                'code' => $Addresses[$i]->city_code,
                'temp' => 0,
                'wheater_data' => (object)[]
            ];
            if ($Addresses[$i]->city_code != null) {
                array_push($citycodes, $row);
            }
        }

        $citycodes = $this->remove_duplicate($citycodes);

        for ($i = 0; $i < count($citycodes); $i++) {
            $code = $citycodes[$i]->code;
            $json = json_decode(file_get_contents("http://api.openweathermap.org/data/2.5/weather?id=$code&units=metric&lang=en&appid=b1ffa48ca579e88c8a13213bbe7cf9bd", true));
            $citycodes[$i]->temp = $json->main->temp;
            $citycodes[$i]->wheater_data = $json;
        }

        return response()->json($citycodes, 200);
    }

    protected function remove_duplicate($array)
    {
        $models = array_map(function ($item) {
            return $item->code;
        }, $array);

        $unique_models = array_unique($models);

        return array_values(array_intersect_key($array, $unique_models));
    }

    public function getChartsData()
    {
        $user = Auth::user();

        $Pumps_online = [];
        $Pumps_offline = [];
        $All_working_pumps = [];

        switch ($user->role_id) {
            case 1:
                $Pumps_online = Pump::where([['online', '=', 1]])->get();
                $Pumps_offline = Pump::where([['online', '=', 0]])->get();
                $All_working_pumps = Pump::where([['temperature', '!=', 0]])->get();

                break;
            case 2:
                $Pumps_online = Pump::where([['online', '=', 1]])->get();
                $Pumps_offline = Pump::where([['online', '=', 0]])->get();
                $All_working_pumps = Pump::where([['temperature', '!=', 0]])->get();

                break;
            case 3:
                if ($user->company_id != null) {
                    $Users = User::where([['company_id', '=', $user->company_id]])->get();

                    for ($i = 0; $i < count($Users); $i++) {

                        if ($Users[$i]->pump_id != null) {
                            $Online = Pump::where([['id', '=', $Users[$i]->pump_id], ['online', '=', 1]])->get();
                            $Offline = Pump::where([['id', '=', $Users[$i]->pump_id], ['online', '=', 0]])->get();
                            $Working_pumps = Pump::where([['id', '=', $Users[$i]->pump_id], ['temperature', '!=', 0]])->get();

                            if (count($Online) != 0) {
                                array_push($Pumps_online, $Online);
                            }

                            if (count($Offline) != 0) {
                                array_push($Pumps_offline, $Offline);
                            }

                            if (count($Working_pumps) != 0) {
                                array_push($All_working_pumps, $Working_pumps[0]);
                            }
                        }
                    }
                }
                break;
            case 4:
                break;
            case 5:
                $Pumps = Pump::all();
                break;
        }


        $data = [];
        array_push($data, count($Pumps_online));
        array_push($data, count($Pumps_offline));

        $Pumps_temp = [];
        $Pump_temp_labels = [];
        $Pumps_set_temp = [];

        for ($i = 0; $i < count($All_working_pumps); $i++) {
            array_push($Pumps_temp, $All_working_pumps[$i]->temperature);
            array_push($Pump_temp_labels, $All_working_pumps[$i]->id);
            array_push($Pumps_set_temp, $All_working_pumps[$i]->set_temp);
        }

        $response = (object)[
            'online_pumps' => $data,
            'pumps_temp' => $Pumps_temp,
            'pump_temp_labels' => $Pump_temp_labels,
            'pumps_set_temp' => $Pumps_set_temp
        ];
        return  response()->json($response, 200);
    }

    public function auto()
    {
        $Pumps = Pump::all();

        for ($i = 0; $i < count($Pumps); $i++) {
            $Address = Address::find($Pumps[$i]->address_id);

            if ($Address) {
                $city_code = $Address->city_code;
                error_log($city_code);

                $json = json_decode(file_get_contents("http://api.openweathermap.org/data/2.5/weather?id=$city_code&units=metric&lang=en&appid=b1ffa48ca579e88c8a13213bbe7cf9bd", true));
                $temp = $json->main->temp;

                $Pumps[$i]->city_temp = $temp;

                if ($Pumps[$i]->auto > 0) {
                    $autoTemp = $this->getTemp($temp, $Pumps[$i]->auto);
                    $Command = Command::where([['pump_id', '=', $Pumps[$i]->id], ['command', '=', 'SET_TEMP']])->first();
                    $Command->temp = $autoTemp;
                    $Command->save();
                }
            }
            $Pumps[$i]->save();

           //sleep(10);
        }
    }


    protected function getTemp($city_wheater, $level)
    {
        if ($level == 1) {
            if ($city_wheater < 22 && $city_wheater >= 15) {
                return 26;
            } else if ($city_wheater < 15 && $city_wheater >= 10) {
                return 27;
            } else if ($city_wheater < 10 && $city_wheater >= 5) {
                return 28;
            } else if ($city_wheater < 5 && $city_wheater >= 0) {
                return 29;
            } else if ($city_wheater < 0 && $city_wheater >= -5) {
                return 30;
            } else if ($city_wheater < -5 && $city_wheater >= -10) {
                return 31;
            } else if ($city_wheater < -5 && $city_wheater >= -10) {
                return 32;
            }
        } else if ($level == 2) {
            if ($city_wheater < 22 && $city_wheater >= 15) {
                return 29;
            } else if ($city_wheater < 15 && $city_wheater >= 10) {
                return 30;
            } else if ($city_wheater < 10 && $city_wheater >= 5) {
                return 31;
            } else if ($city_wheater < 5 && $city_wheater >= 0) {
                return 32;
            } else if ($city_wheater < 0 && $city_wheater >= -5) {
                return 33;
            } else if ($city_wheater < -5 && $city_wheater >= -10) {
                return 34;
            } else if ($city_wheater < -5 && $city_wheater >= -10) {
                return 35;
            }
        } else if ($level == 3) {
            if ($city_wheater < 22 && $city_wheater >= 15) {
                return 31;
            } else if ($city_wheater < 15 && $city_wheater >= 10) {
                return 32;
            } else if ($city_wheater < 10 && $city_wheater >= 5) {
                return 33;
            } else if ($city_wheater < 5 && $city_wheater >= 0) {
                return 34;
            } else if ($city_wheater < 0 && $city_wheater >= -5) {
                return 35;
            } else if ($city_wheater < -5 && $city_wheater >= -10) {
                return 36;
            } else if ($city_wheater < -5 && $city_wheater >= -10) {
                return 37;
            }
        } else if ($level == 4) {
            if ($city_wheater < 22 && $city_wheater >= 15) {
                return 32;
            } else if ($city_wheater < 15 && $city_wheater >= 10) {
                return 34;
            } else if ($city_wheater < 10 && $city_wheater >= 5) {
                return 35.5;
            } else if ($city_wheater < 5 && $city_wheater >= 0) {
                return 38;
            } else if ($city_wheater < 0 && $city_wheater >= -5) {
                return 40;
            } else if ($city_wheater < -5 && $city_wheater >= -10) {
                return 42;
            } else if ($city_wheater < -5 && $city_wheater >= -10) {
                return 44;
            }
        } else if ($level == 5) {
            if ($city_wheater < 22 && $city_wheater >= 15) {
                return 34;
            } else if ($city_wheater < 15 && $city_wheater >= 10) {
                return 36;
            } else if ($city_wheater < 10 && $city_wheater >= 5) {
                return 37;
            } else if ($city_wheater < 5 && $city_wheater >= 0) {
                return 40;
            } else if ($city_wheater < 0 && $city_wheater >= -5) {
                return 42;
            } else if ($city_wheater < -5 && $city_wheater >= -10) {
                return 44;
            } else if ($city_wheater < -5 && $city_wheater >= -10) {
                return 46;
            }
        } else if ($level == 6) {
            if ($city_wheater < 22 && $city_wheater >= 15) {
                return 36;
            } else if ($city_wheater < 15 && $city_wheater >= 10) {
                return 37;
            } else if ($city_wheater < 10 && $city_wheater >= 5) {
                return 38;
            } else if ($city_wheater < 5 && $city_wheater >= 0) {
                return 42;
            } else if ($city_wheater < 0 && $city_wheater >= -5) {
                return 44;
            } else if ($city_wheater < -5 && $city_wheater >= -10) {
                return 46;
            } else if ($city_wheater < -5 && $city_wheater >= -10) {
                return 48;
            }
        } else {
            if ($city_wheater < 22 && $city_wheater >= 15) {
                return 38;
            } else if ($city_wheater < 15 && $city_wheater >= 10) {
                return 39;
            } else if ($city_wheater < 10 && $city_wheater >= 5) {
                return 40;
            } else if ($city_wheater < 5 && $city_wheater >= 0) {
                return 42;
            } else if ($city_wheater < 0 && $city_wheater >= -5) {
                return 44;
            } else if ($city_wheater < -5 && $city_wheater >= -10) {
                return 46;
            } else if ($city_wheater < -5 && $city_wheater >= -10) {
                return 50;
            }
        }
        return 0;
    }
}
