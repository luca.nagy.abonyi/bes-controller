<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\User;
use App\Address;
use Illuminate\Http\Request;
use App\Role;
use App\Company;

class UserController extends Controller
{
    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    /* public function __construct()
    {
        $this->middleware('auth');
    } */

    /**
     * Get the authenticated User.
     *
     * @return Response
     */
    public function profile()
    {
        $user = Auth::user();

        if ($user) {
            if ($user->address_id) {
                $address = Address::find($user->address_id);
                $user->address = $address;
            }


            if ($user->company_id) {
                $company = Company::find($user->company_id);
                if ($company->address_id) {
                    $address = Address::find($company->address_id);
                    $company->address = $address;
                }
                $user->company = $company;
            } else {
                $user->company = (object)[];
            }
        }
        return response()->json($user, 200);
    }

    /**
     * Get all User.
     *
     * @return Response
     */
    public function allUsers()
    {
        $Users = User::all();

        for ($i = 0; $i < count($Users); $i++) {
            $Address = Address::find($Users[$i]->address_id);
            $Users[$i]->address = $Address;
        }
        return response()->json(['users' =>  $Users], 200);
    }

    public function getCustomers()
    {
        $user = Auth::user();
        switch ($user->role_id) {
            case 1:
                $Users = User::where([['role_id', '=', 4]])->get();
                break;
            case 2:
                $Users = User::where([['role_id', '=', 4]])->get();
                break;
            case 3:
                $Users = User::where([['company_id', '=', $user->company_id], ['role_id', '=', 4]])->get();
                break;
        }

        for ($i = 0; $i < count($Users); $i++) {
            $Address = Address::find($Users[$i]->address_id);
            $Users[$i]->address = $Address;
            if ($Users[$i]->company_id) {
                $company = Company::find($Users[$i]->company_id);
                $Users[$i]->company = $company->name;
            } else {
                $Users[$i]->company = '-';
            }
        }
        return response()->json(['users' =>  $Users], 200);
    }

    public function getWorkers()
    {
        $Users = User::where([['role_id', '=', 5]])->get();

        return response()->json($Users, 200);
    }

    public function getUsers()
    {

        $Users = User::where([['role_id', '!=', 4]])->get();
        for ($i = 0; $i < count($Users); $i++) {
            $Address = Address::find($Users[$i]->address_id);
            $Users[$i]->address = $Address;
            $Role = Role::find($Users[$i]->role_id);
            $Users[$i]->role = $Role->name;
            if ($Users[$i]->company_id) {
                $Users[$i]->company = Company::find($Users[$i]->company_id);
            }
        }
        return response()->json($Users, 200);
    }


    public function update($id, Request $request)
    {
        $User = User::find($id);
        if ($request->input('password')) {
            $pass =  app('hash')->make($request->input('password'));
            $User->password = $pass;
        }
        $User->update($request->all());

        return response()->json($User, 200);
    }

    /**
     * Get one user.
     *
     * @return Response
     */
    public function singleUser($id)
    {
        try {
            $user = User::findOrFail($id);
            if ($user->address_id !== null) {
                $address = Address::find($user->address_id);

                $user->address = $address;
            }

            return response()->json(['user' => $user], 200);
        } catch (\Exception $e) {

            return response()->json(['message' => 'user not found!'], 404);
        }
    }

    public function resetPassword($id)
    {
        $User = User::find($id);
        $User->password = app('hash')->make('123456');
        $User->save();

        return response("Success!", 200);
    }

    public function getAdmins()
    {
        $Users = User::where([['role_id', '<', 4]])->get();

        return  response()->json($Users, 200);
    }

    public function getRoles()
    {
        $Roles = Role::where([['id', '!=', 4]])->get();

        return  response()->json($Roles, 200);
    }
}
