<?php

namespace App\Http\Controllers;

use App\UserPump;
use Illuminate\Http\Request;

class UserPumpController extends Controller
{
    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showAllUserPumps()
    {
        return response()->json(UserPump::all());
    }

    public function showOneUserPump($id)
    {
        return response()->json(UserPump::find($id));
    }

    public function create(Request $request)
    {
        $UserPump = UserPump::create($request->all());

        return response()->json($UserPump, 201);
    }

    public function update($id, Request $request)
    {
        $UserPump = UserPump::findOrFail($id);
        $UserPump->update($request->all());

        return response()->json($UserPump, 200);
    }

    public function delete($id)
    {
        UserPump::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}
