<?php

namespace App\Http\Controllers;

use App\Service;
use App\User;
use App\Pump;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ServiceController extends Controller
{
    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showAllServices()
    {
        $user = Auth::user();
        switch ($user->role_id) {
            case 1:
                $Services = Service::all();
                break;
            case 2:
                $Services = Service::all();
                break;
            case 3:
                $Users = User::where([['company_id', '=', $user->company_id]])->get();
                $Pumps = [];
                $Services = [];
                for ($i = 0; $i < count($Users); $i++) {
                    $Pump = Pump::find($Users[$i]->pump_id);
                    if ($Pump != null) {
                        array_push($Pumps, $Pump);
                    }
                }

                if (count($Pumps) != 0) {
                    for ($i = 0; $i < count($Pumps); $i++) {
                        $Service = Service::where([['pump_id', '=', $Pumps[$i]->id]])->get();
                        for ($j = 0; $j < count($Service); $j++) {
                            array_push($Services, $Service[$j]);
                        }
                    }
                }
                break;
            case 5:
                $Services = Service::all();
        }

        //$Services = Service::all();
        for ($i = 0; $i < count($Services); $i++) {
            $User = User::find($Services[$i]->user_id);
            $Services[$i]->user_name = $User->name;
        }
        return response()->json($Services);
    }

    public function showOneService($id)
    {
        return response()->json(Service::find($id));
    }

    public function create(Request $request)
    {
        $Service = Service::create($request->all());

        return response()->json($Service, 201);
    }

    public function update($id, Request $request)
    {
        $Service = Service::findOrFail($id);
        $Service->update($request->all());

        return response()->json($Service, 200);
    }

    public function delete($id)
    {
        Service::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}
