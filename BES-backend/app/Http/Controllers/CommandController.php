<?php

namespace App\Http\Controllers;

use App\Command;
use App\Pump;
use Illuminate\Http\Request;

class CommandController extends Controller
{
    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */


    public function showAllPumps()
    {
        return response()->json(Command::all());
    }

    public function showOnePump($id)
    {
        return response()->json(Command::find($id));
    }

    public function create(Request $request)
    {
        $Command = Command::create($request->all());

        return response()->json($Command, 201);
    }

    public function update($id, Request $request)
    {
        $Command = Command::findOrFail($id);
        $Command->update($request->all());

        return response()->json($Command, 200);
    }


    public function setTemp($id, Request $request)
    {
        $Command = Command::where([['pump_id', '=', $id], ['command', '=', 'SET_TEMP']])->first();
        $Pump = Pump::find($id);
        $Pump->previuos_temperature = $Pump->temperature;
        $Pump->set_temp = $request->temp;
        $Pump->save();
        $Command->temp =  $request->temp;
        $Command->readed = 0;
        $Command->save();
        return response()->json($Command, 200);
    }

    public function cool($id)
    {
        $Command = Command::where([['pump_id', '=', $id], ['command', '=', 'COOL']])->first();

        $Command->readed = 0;
        $Command->save();
        return response()->json($Command, 200);
    }

    public function heat($id)
    {
        $Command = Command::where([['pump_id', '=', $id], ['command', '=', 'HEAT']])->first();

        $Command->readed = 0;
        $Command->save();
        return response()->json($Command, 200);
    }

    public function reset($id)
    {
        $Command = Command::where([['pump_id', '=', $id], ['command', '=', 'RESET']])->first();

        $Command->readed = 0;
        $Command->save();
        return response()->json($Command, 200);
    }

    public function auto($level)
    {
    }

    public function delete($id)
    {
        Command::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}
