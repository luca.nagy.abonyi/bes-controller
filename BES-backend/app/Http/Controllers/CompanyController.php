<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Http\Request;
use App\User;
use App\Address;
use Illuminate\Support\Facades\Auth;

class CompanyController extends Controller
{
    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    /*  public function __construct()
    {
        $this->middleware('auth');
    } */

    public function showAllCompanies()
    {
        $user = Auth::user();
        switch ($user->role_id) {
            case 1:
                $Companies = Company::all();
                break;
            case 2:
                $Companies = Company::all();
                break;
            case 3:
                $Companies = Company::where([['id', '=', $user->company_id]])->get();
                break;
            case 5:
                $Companies = [];
            }
        for ($i = 0; $i < sizeof($Companies); $i++) {
            $Address = Address::find($Companies[$i]->address_id);
            $User = User::where([['company_id', '=', $Companies[$i]->id], ['role_id', '<', 4]])->first();
            $Companies[$i]->address = $Address;
            $Companies[$i]->admin = $User;
        }
        return response()->json($Companies);
    }

    public function showOneCompany($id)
    {
        $Company = Company::find($id);
        $Address = Address::find($Company->address_id);
        $User = User::where([['company_id', '=', $Company->id], ['role_id', '<', 4]])->first();
        $Company->address = $Address;
        $Company->admin = $User;

        return response()->json($Company);
    }

    public function create(Request $request)
    {
        $Address = Address::create([
            "zip_code" => $request->zip_code,
            "city" => $request->city,
            "city_code" => $request->city_code,
            "address" => $request->address,
            "coordinate" => '',
            "country" => $request->country
        ]);
        error_log($Address);

        $Company = Company::create([
            "name" => $request->name,
            "phone" => $request->phone,
            "email" => $request->email,
            "address_id" => $Address->id
        ]);

        $User = User::find($request->user_id);
        $User->company_id = $Company->id;
        $User->save();

        return response()->json($Company, 201);
    }

    public function update($id, Request $request)
    {
        $Company = Company::findOrFail($id);
        $Company->email = $request->email;
        $Company->name = $request->name;
        $Company->phone = $request->phone;

        $Address = Address::find($Company->address_id);
        $Address->country = $request->country;
        $Address->city = $request->city;
        $Address->address = $request->address;
        $Address->zip_code = $request->zip_code;
        $Address->city_code = $request->city_code;


        $User = User::where([['company_id','=', $Company->id], ['role_id', '<', 4]])->first();
        if($User){
            $User->company_id = null;
            $User->save();

        }

        $Admin = User::find($request->user_id);
        if($Admin){
            $Admin->company_id = $Company->id;
            $Admin->save();

        }

        $Company->save();
        $Address->save();

        return response()->json($Company, 200);
    }

    public function delete($id)
    {
        Company::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}
