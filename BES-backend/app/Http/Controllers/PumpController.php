<?php

namespace App\Http\Controllers;

use App\Pump;
use App\User;
use App\Address;
use App\Company;
use Illuminate\Http\Request;
use App\Command;
use Illuminate\Support\Facades\Auth;

class PumpController extends Controller
{
    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    /* public function __construct()
    {
        $this->middleware('auth');
    } */

    public function showAllPumps()
    {
        $user = Auth::user();
        $Pumps = [];

        switch ($user->role_id) {
            case 1:
                $Pumps = Pump::all();
                break;
            case 2:
                $Pumps = Pump::all();
                break;
            case 3:
                if ($user->company_id != null) {
                    $Users = User::where([['company_id', '=', $user->company_id]])->get();

                    for ($i = 0; $i < count($Users); $i++) {
                        if ($Users[$i]->pump_id != null) {
                            $Pump = Pump::findOrFail($Users[$i]->pump_id);
                            array_push($Pumps, $Pump);
                        }
                    }
                }
                break;
            case 4:
                break;
            case 5:
                $Pumps = Pump::all();
                break;
        }

        for ($i = 0; $i < count($Pumps); $i++) {
            $Heartbeat = Command::where([['pump_id', '=', $Pumps[$i]->id], ['command', '=', 'HEARTBEAT']])->first();
            $lastonline = strtotime($Heartbeat->read_at);

            if ($lastonline) {
                if (
                    date('d') == date('d', $lastonline) && date('m') == date('m', $lastonline)
                    && date('H') == date('H', $lastonline)
                ) {
                    $Pumps[$i]->online = 1;
                } else {
                    $Pumps[$i]->online = 0;
                }
            } else {
                $Pumps[$i]->online = 0;
            }

            $Pumps[$i]->save();
            if ($Pumps[$i]->address_id != null) {
                $Address = Address::find($Pumps[$i]->address_id);
                $Pumps[$i]->address = $Address;
            }

            $User = User::where('pump_id', '=',$Pumps[$i]->id)->first();
            $Pumps[$i]->user = $User;

            if ($User != null) {
                if ($User->company_id != null) {
                    $Company = Company::find($User->company_id);
                    $Pumps[$i]->company = $Company->name;
                }
            }
        }
        return response()->json($Pumps, 200);
    }

    public function showOnePump($id)
    {
        $Pump = Pump::find($id);
        $Address = Address::find($Pump->address_id);
        $Pump->address = $Address;

        $User = User::where([['pump_id', '=', $Pump->id]])->first();
        $Pump->user = $User;
        return response()->json($Pump);
    }

    public function create(Request $request)
    {
        $Address = Address::create([
            "zip_code" => $request->zip_code,
            "city" => $request->city,
            "city_code" => $request->city_code,
            "address" => $request->address,
            "coordinate" => $request->coordinate,
            "country" => $request->country
        ]);

        $Pump = Pump::create([
            "status" => 1,
            "set_temp"  => 30,
            "temp" => 30,
            "free"  =>  0,
            "temperature"  =>  0,
            "previuos_temperature"  =>  0,
            "free"  =>  0,
            "online"  =>  0,
            "city_temp"  =>  0,
            "MAC" => $request->MAC,
            "address_id" => $Address->id
        ]);

        $CommandSET = Command::create([
            "pump_id" => $Pump->id,
            "command" => "SET_TEMP",
            "readed" => 1,
            "temp" => 0
        ]);

        $CommandCOOL = Command::create([
            "pump_id" => $Pump->id,
            "command" => "COOL",
            "readed" => 1,
            "temp" => 0
        ]);

        $CommandHEAT = Command::create([
            "pump_id" => $Pump->id,
            "command" => "HEAT",
            "readed" => 1,
            "temp" => 0
        ]);

        $CommandRESET = Command::create([
            "pump_id" => $Pump->id,
            "command" => "RESET",
            "readed" => 1,
            "temp" => 0
        ]);

        $CommandHEARTBEAT = Command::create([
            "pump_id" => $Pump->id,
            "command" => "HEARTBEAT",
            "readed" => 1,
            "temp" => 0
        ]);

        return response()->json($Pump, 201);
    }

    public function update($id, Request $request)
    {
        $Pump = Pump::findOrFail($id);
        $Address = Address::find($Pump->address_id);
        $User = User::find($request->input('user_id'));

        $Address->city = $request->city;
        $Address->zip_code = $request->zip_code;
        $Address->address = $request->address;
        $Address->coordinate = $request->coordinate;
        $Address->country = $request->country;
        $Address->city_code = $request->city_code;

        $Address->save();

        $User->address_id = $Address->id;
        $User->pump_id = $id;
        $User->save();

        $Pump->MAC = $request->input('MAC');
        $Pump->auto = $request->input('auto');
        $Pump->free = $request->input('free');

        $Pump->save();
    }

    public function auto(Request $request)
    {
        $Pump = Pump::findOrFail($request->id);
        $Pump->auto = $request->auto;
        $Pump->set_temp = $request->set_temp;
        $Pump->save();

        return response()->json($Pump, 200);
    }

    public function delete($id)
    {
        $Pump = Pump::findOrFail($id);
        $Address = Address::find($Pump->address_id);
        $Pump->delete();
        $Address->delete();
        return response('Deleted Successfully', 200);
    }
}
