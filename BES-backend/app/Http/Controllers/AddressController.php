<?php

namespace App\Http\Controllers;

use App\Address;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showAllAddresses()
    {
        return response()->json(Address::all());
    }

    public function showOneAddress($id)
    {
        return response()->json(Address::find($id));
    }

    public function create(Request $request)
    {
        $Address = Address::create($request->all());

        return response()->json($Address, 201);
    }

    public function update($id, Request $request)
    {
        $Address = Address::findOrFail($id);
        $Address->update($request->all());

        return response()->json($Address, 200);
    }

    public function delete($id)
    {
        Address::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}
