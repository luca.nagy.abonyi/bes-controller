<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use  App\User;

class AuthController extends Controller
{
    /**
     * Store a new user.
     *
     * @param  Request  $request
     * @return Response
     */
    public function register(Request $request)
    {
        //validate incoming request
        $this->validate($request, [
            'name' => 'required|string',
            'phone' => 'required|string|unique:users',
            'password' => 'required|confirmed',
            'email' => 'string',
        ]);

        try {
            $user = new User;
            $user->name = $request->input('name');
            $user->phone = $request->input('phone');
            $plainPassword = $request->input('password');
            $user->password = app('hash')->make($plainPassword);
            $user->email = $request->input('email');
            $user->role_id = $request->input('role_id');
            $user->company_id = $request->input('company_id');

            $user->save();

            //return successful response
            return response()->json(['user' => $user, 'message' => 'CREATED'], 201);
        } catch (\Exception $e) {
            //return error message

            error_log($e);

            return response()->json(['message' => $e], 409);
        }
    }

    /**
     * Get a JWT via given credentials.
     *
     * @param  Request  $request
     * @return Response
     */
    public function login(Request $request)
    {
        //validate incoming request
        $this->validate($request, [
            'phone' => 'required|string',
            'password' => 'required|string',
        ], [
            'phone.required' => 'This field is required.',
            'password.required' => 'This field is required.',
        ]);

        $credentials = $request->only(['phone', 'password']);

        if (!$token = Auth::attempt($credentials)) {
            return response()->json(['message' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }

    public function adminLogin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|string',
            'password' => 'required|string',
        ], [
            'email.required' => 'This field is required.',
            'password.required' => 'This field is required.',
        ]);

        $credentials = $request->only(['email', 'password']);

        if (!$token = Auth::attempt($credentials)) {
            return response()->json(['message' => 'The email or the password is invalid.'], 401);
        }

        return $this->respondWithToken($token);
    }

}
