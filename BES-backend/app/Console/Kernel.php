<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;
use App\Pump;
use App\Command;
use App\Address;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            $Pumps = Pump::all();

            for ($i = 0; $i < count($Pumps); $i++) {
                $Address = Address::find($Pumps[$i]->address_id);

                if ($Address->city_code) {
                    $city_code = $Address->city_code;

                    $json = json_decode(file_get_contents("http://api.openweathermap.org/data/2.5/weather?id=$city_code&units=metric&lang=en&appid=b1ffa48ca579e88c8a13213bbe7cf9bd", true));
                    $temp = $json->main->temp;

                    $Pumps[$i]->city_temp = $temp;

                    if ($Pumps[$i]->auto > 0) {
                        $autoTemp = $this->getTemp($temp, $Pumps[$i]->auto);
                        $Command = Command::where([['pump_id', '=', $Pumps[$i]->id], ['command', '=', 'SET_TEMP']])->first();
                        $Command->temp = $autoTemp;
                        $Command->save();
                    }
                }
                $Pumps[$i]->save();

                $Heartbeat = Command::where([['pump_id', '=', $Pumps[$i]->id], ['command', '=', 'HEARTBEAT']])->first();
                $lastonline = strtotime($Heartbeat->read_at);

                if ($lastonline) {
                    if (
                        date('d') == date('d', $lastonline) && date('m') == date('m', $lastonline)
                        && date('H') == date('H', $lastonline)
                    ) {
                        $Pumps[$i]->online = 1;
                    } else {
                        $Pumps[$i]->online = 0;
                    }
                } else {
                    $Pumps[$i]->online = 0;
                }
            }

            sleep(10);
        })->hourly();
    }

    protected function getTemp($city_wheater, $level)
    {
        if ($level == 1) {
            if ($city_wheater < 22 && $city_wheater >= 15) {
                return 26;
            } else if ($city_wheater < 15 && $city_wheater >= 10) {
                return 27;
            } else if ($city_wheater < 10 && $city_wheater >= 5) {
                return 28;
            } else if ($city_wheater < 5 && $city_wheater >= 0) {
                return 29;
            } else if ($city_wheater < 0 && $city_wheater >= -5) {
                return 30;
            } else if ($city_wheater < -5 && $city_wheater >= -10) {
                return 31;
            } else if ($city_wheater < -5 && $city_wheater >= -10) {
                return 32;
            }
        } else if ($level == 2) {
            if ($city_wheater < 22 && $city_wheater >= 15) {
                return 29;
            } else if ($city_wheater < 15 && $city_wheater >= 10) {
                return 30;
            } else if ($city_wheater < 10 && $city_wheater >= 5) {
                return 31;
            } else if ($city_wheater < 5 && $city_wheater >= 0) {
                return 32;
            } else if ($city_wheater < 0 && $city_wheater >= -5) {
                return 33;
            } else if ($city_wheater < -5 && $city_wheater >= -10) {
                return 34;
            } else if ($city_wheater < -5 && $city_wheater >= -10) {
                return 35;
            }
        } else if ($level == 3) {
            if ($city_wheater < 22 && $city_wheater >= 15) {
                return 31;
            } else if ($city_wheater < 15 && $city_wheater >= 10) {
                return 32;
            } else if ($city_wheater < 10 && $city_wheater >= 5) {
                return 33;
            } else if ($city_wheater < 5 && $city_wheater >= 0) {
                return 34;
            } else if ($city_wheater < 0 && $city_wheater >= -5) {
                return 35;
            } else if ($city_wheater < -5 && $city_wheater >= -10) {
                return 36;
            } else if ($city_wheater < -5 && $city_wheater >= -10) {
                return 37;
            }
        } else if ($level == 4) {
            if ($city_wheater < 22 && $city_wheater >= 15) {
                return 32;
            } else if ($city_wheater < 15 && $city_wheater >= 10) {
                return 34;
            } else if ($city_wheater < 10 && $city_wheater >= 5) {
                return 35.5;
            } else if ($city_wheater < 5 && $city_wheater >= 0) {
                return 38;
            } else if ($city_wheater < 0 && $city_wheater >= -5) {
                return 40;
            } else if ($city_wheater < -5 && $city_wheater >= -10) {
                return 42;
            } else if ($city_wheater < -5 && $city_wheater >= -10) {
                return 44;
            }
        } else if ($level == 5) {
            if ($city_wheater < 22 && $city_wheater >= 15) {
                return 34;
            } else if ($city_wheater < 15 && $city_wheater >= 10) {
                return 36;
            } else if ($city_wheater < 10 && $city_wheater >= 5) {
                return 37;
            } else if ($city_wheater < 5 && $city_wheater >= 0) {
                return 40;
            } else if ($city_wheater < 0 && $city_wheater >= -5) {
                return 42;
            } else if ($city_wheater < -5 && $city_wheater >= -10) {
                return 44;
            } else if ($city_wheater < -5 && $city_wheater >= -10) {
                return 46;
            }
        } else if ($level == 6) {
            if ($city_wheater < 22 && $city_wheater >= 15) {
                return 36;
            } else if ($city_wheater < 15 && $city_wheater >= 10) {
                return 37;
            } else if ($city_wheater < 10 && $city_wheater >= 5) {
                return 38;
            } else if ($city_wheater < 5 && $city_wheater >= 0) {
                return 42;
            } else if ($city_wheater < 0 && $city_wheater >= -5) {
                return 44;
            } else if ($city_wheater < -5 && $city_wheater >= -10) {
                return 46;
            } else if ($city_wheater < -5 && $city_wheater >= -10) {
                return 48;
            }
        } else {
            if ($city_wheater < 22 && $city_wheater >= 15) {
                return 38;
            } else if ($city_wheater < 15 && $city_wheater >= 10) {
                return 39;
            } else if ($city_wheater < 10 && $city_wheater >= 5) {
                return 40;
            } else if ($city_wheater < 5 && $city_wheater >= 0) {
                return 42;
            } else if ($city_wheater < 0 && $city_wheater >= -5) {
                return 44;
            } else if ($city_wheater < -5 && $city_wheater >= -10) {
                return 46;
            } else if ($city_wheater < -5 && $city_wheater >= -10) {
                return 50;
            }
        }
        return 0;
    }
}
