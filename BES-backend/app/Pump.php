<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DateTimeInterface;

class Pump extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'status', 'set_temp', 'temperature', 'previuos_temperature','address_id', 'free', 'online', 'city_temp', 'manual', 'auto', 'MAC', 'company_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
