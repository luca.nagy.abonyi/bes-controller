<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api'], function () use ($router) {
    $router->post('register', 'AuthController@register');

    $router->post('login', 'AuthController@login');

    $router->post('admin-login', 'AuthController@adminLogin');

    $router->get('pumps',  ['uses' => 'PumpController@showAllPumps']);

    $router->get('pumps/{id}', ['uses' => 'PumpController@showOnePump']);

    $router->post('pumps', ['uses' => 'PumpController@create']);

    $router->delete('pumps/{id}', ['uses' => 'PumpController@delete']);

    $router->put('pumps/auto', ['uses' => 'PumpController@auto']);

    $router->put('pumps/{id}', ['uses' => 'PumpController@update']);

    $router->get('commands',  ['uses' => 'CommandController@showAllPumps']);

    $router->get('commands/{id}', ['uses' => 'CommandController@showOnePump']);

    $router->post('commands', ['uses' => 'CommandController@create']);

    $router->delete('commands/{id}', ['uses' => 'CommandController@delete']);

    $router->put('commands/{id}', ['uses' => 'CommandController@update']);

    $router->put('commands/setTemp/{id}', ['uses' => 'CommandController@setTemp']);
    $router->put('commands/cool/{id}', ['uses' => 'CommandController@cool']);
    $router->put('commands/heat/{id}', ['uses' => 'CommandController@heat']);
    $router->put('commands/reset/{id}', ['uses' => 'CommandController@reset']);
    $router->put('commands/auto/{level}',['uses' => 'CommandController@auto']);

    $router->get('addresses',  ['uses' => 'AddressController@showAllAddresses']);

    $router->get('addresses/{id}', ['uses' => 'AddressController@showOneAddress']);

    $router->post('addresses', ['uses' => 'AddressController@create']);

    $router->delete('addresses/{id}', ['uses' => 'AddressController@delete']);

    $router->put('addresses/{id}', ['uses' => 'AddressController@update']);

    $router->get('user_pumps',  ['uses' => 'UserPumpController@showAllUserPumps']);

    $router->get('user_pumps/{id}', ['uses' => 'UserPumpController@showOneUserPump']);

    $router->post('user_pumps', ['uses' => 'UserPumpController@create']);

    $router->delete('user_pumps/{id}', ['uses' => 'UserPumpController@delete']);

    $router->put('user_pumps/{id}', ['uses' => 'UserPumpController@update']);

    $router->get('profile', 'UserController@profile');

    $router->get('users/{id}', 'UserController@singleUser');

    $router->get('users', 'UserController@allUsers');

    $router->get('all-users', 'UserController@getUsers');

    $router->get('roles', 'UserController@getRoles');

    $router->get('customers', 'UserController@getCustomers');

    $router->get('workers', 'UserController@getWorkers');

    $router->get('admins', 'UserController@getAdmins');

    $router->put('users/{id}', 'UserController@update');

    $router->get('companies',  ['uses' => 'CompanyController@showAllCompanies']);

    $router->get('companies/{id}', ['uses' => 'CompanyController@showOneCompany']);

    $router->post('companies', ['uses' => 'CompanyController@create']);

    $router->put('companies/{id}', ['uses' => 'CompanyController@update']);

    $router->delete('companies/{id}', ['uses' => 'CompanyController@delete']);

    $router->get('services',  ['uses' => 'ServiceController@showAllServices']);

    $router->get('services/{id}', ['uses' => 'ServiceController@showOneService']);

    $router->post('services', ['uses' => 'ServiceController@create']);

    $router->put('services', ['uses' => 'ServiceController@update']);

    $router->delete('services/{id}', ['uses' => 'ServiceController@delete']);

    $router->get('charts-auto',  ['uses' => 'ChartController@auto']);

    $router->get('city_temps',  ['uses' => 'ChartController@getCitiesTemp']);

    $router->get('charts-data',  ['uses' => 'ChartController@getChartsData']);

    $router->put('reset-password/{id}', 'UserController@resetPassword');


});
