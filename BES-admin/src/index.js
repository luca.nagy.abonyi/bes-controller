import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {createStore} from 'redux';
import rootReducer from './reducers/index';
import { ThemeProvider} from '@material-ui/core/styles';
import { createTheme } from '@material-ui/core/styles';

const theme = createTheme ({
  palette: {
    primary: {
      light: '#9ABCA7',
      main: '#9ABCA7',
      dark: '#9ABCA7',
      contrastText: '#fff',
    },
    secondary: {
      light: '#840032',
      main: '#840032',
      dark: '#840032',
      contrastText: '#fff',
    },
  },
});

const store = createStore (rootReducer);
export default store;

ReactDOM.render (
    <ThemeProvider theme={theme}>
      <App />
    </ThemeProvider>,
  document.getElementById ('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals ();
