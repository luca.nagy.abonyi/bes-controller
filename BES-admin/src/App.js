import './App.css';
import {Redirect} from 'react-router';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Home from './views/Home';
import Sidenavbar from './components/Sidenavbar';
import Pumps from './views/Pumps';
import Customers from './views/Customers';
import Login from './views/Login';
import Services from './views/Services';
import Users from './views/Users';
import Middleware from './components/Middleware';
import Profile from './views/Profile';
import Companies from './views/Companies';

const ProtectedRoute = ({component: Component, ...rest}) => (
  <Route
    {...rest}
    render={props =>
      localStorage.token !== ''
        ? <div>
            <Sidenavbar />
            <Component {...props} />
          </div>
        : <Redirect
            to={{
              pathname: '/login',
              state: {from: props.location},
            }}
          />}
  />
);

function App () {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path="/login" component={Login} exact />
          <ProtectedRoute path="/home" component={Home} exact />
          <ProtectedRoute path="/pumps" component={Pumps} exact />
          <ProtectedRoute path="/customers" component={Customers} exact />
          <ProtectedRoute path="/services" component={Services} exact />
          <ProtectedRoute path="/users" component={Users} exact />
          <ProtectedRoute path="/profile" component={Profile} exact />
          <ProtectedRoute path="/companies" component={Companies} exact />
          <Route path="/" component={Middleware} exact />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
