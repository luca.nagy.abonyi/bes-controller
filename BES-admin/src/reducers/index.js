import {AUTH} from '../actions/types';

const initialState = {
  auth: localStorage.getItem ('auth'),
};

function rootReducer (state = initialState, action) {
  if (action.type === AUTH) {
    return Object.assign ({}, state, {
      auth: action.payload,
    });
  }
  return state;
}

export default rootReducer;
