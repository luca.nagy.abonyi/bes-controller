import {AUTH} from './types';

export function auth(payload) {
    return { type: AUTH, payload };
  }