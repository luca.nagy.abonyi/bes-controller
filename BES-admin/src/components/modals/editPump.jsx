import React, { Component } from 'react';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import PumpService from '../../services/pumpService';
import CircularProgress from '@mui/material/CircularProgress';
import UserService from '../../services/userService';
import hungarianCities from '../../assets/json/hungarianCities.json';
import serbianCities from '../../assets/json/serbianCities.json';
import Select from 'react-select'
import * as Mui from '@mui/material/';

export default class EditPump extends Component {

    constructor(props) {
        super(props)
        this.state = {
            address: '',
            zip_code: '',
            city: '',
            city_code: 0,
            user_id: 0,
            user: {},
            users: [],
            MAC: '',
            coordinate: '',
            set_temp: '',
            auto: 0,
            free: 0,
            country: '',
            cities: serbianCities,
            defaultCountry: {},
            defaultCity: {}
        }
    }

    componentDidMount() {
        this.getData()
    }

    async getData() {
        await UserService.getUsers().then(res => {
            let users = []
            users[0] = {
                id: 0,
                name: '-'
            }
            res.data.users.forEach(user => {
                users.push(user)

            })
            this.setState({
                users: users
            })
        })

        await PumpService.getPump(this.props.pump_id).then(res => {
            let user_id

            if (res.data.user) {
                user_id = res.data.user.id
            } else {
                user_id = 0
            }

            let defaultCountry = {
                label: res.data.address.country === 'HU' ? 'Hungary' : 'Serbia',
                value: res.data.address.country === 'HU' ? 'HU' : 'RS',
            }
            let cityName = ''

            if (res.data.address.country === 'HU') {
                hungarianCities.forEach(city => {
                    if (city.value === parseInt(res.data.address.city_code)) {
                        cityName = city.label
                    }
                })
            } else {
                serbianCities.forEach(city => {
                    if (city.value === parseInt(res.data.address.city_code)) {
                        cityName = city.label
                    }
                })
            }   

            let defaultCity = {
                label: cityName,
                value: res.data.address.city_code ? res.data.address.city_code : '',
            }

            this.setState({
                address: res.data.address ? res.data.address.address : '',
                zip_code: res.data.address ? res.data.address.zip_code : '',
                city: res.data.address ? res.data.address.city : '',
                user: res.data.user ? res.data.user : '',
                pump_id: this.props.pump_id,
                user_id: user_id,
                MAC: res.data.MAC,
                coordinate: res.data.address ? res.data.address.coordinate : '',
                set_temp: res.data.temperature,
                auto: res.data.auto,
                free: res.data.free,
                city_code: res.data.address ? res.data.address.city_code : '',
                country: res.data.address.country ? res.data.address.country : '',
                defaultCountry: defaultCountry,
                defaultCity: defaultCity
            })
        })
    }

    changeZip(e) {
        this.setState({
            zip_code: e.target.value
        })
    }

    changeCity(e) {
        this.setState({
            city: e.label,
            city_code: e.value
        })
    }

    changeAddress(e) {
        this.setState({
            address: e.target.value

        })
    }

    changeUser(e) {
        this.setState({
            user_id: e.target.value
        })
    }

    changeMAC(e) {
        this.setState({
            MAC: e.target.value
        })
    }

    changeCoordinate(e) {
        this.setState({
            coordinate: e.target.value
        })
    }

    changeSetTemp(e) {
        this.setState({
            set_temp: e.target.value
        })
    }

    changeAuto(e) {
        this.setState({
            auto: e.target.value
        })
    }

    changeFree(e) {
        this.setState({
            free: e.target.value
        })
    }

    async editPump() {
        let pump = {
            address: this.state.address,
            zip_code: this.state.zip_code,
            city: this.state.city,
            city_code: this.state.city_code,
            user_id: this.state.user_id,
            MAC: this.state.MAC,
            coordinate: this.state.coordinate,
            set_temp: this.state.temperature,
            auto: this.state.auto,
            free: this.state.free
        }
        console.log(pump)
        await PumpService.update(this.props.pump_id, pump).then(res => {
            this.props.close()
            this.props.edit()
        })
    }

    setCountry(e) {
        if (e.value === 'RS') {
            this.setState({
                cities: serbianCities,
            })
        } else {
            this.setState({
                cities: hungarianCities,
            })
        }
    }

    render() {
        if (this.state.pump_id !== undefined) {
            return (
                <div className="modal">
                    <div className="modal-title">
                        <h1>Edit pump</h1>
                        <div className="modal-close" onClick={() => this.props.close()}>x</div>
                    </div>
                    <div className="fields-container">
                        <div className="field">
                            <div className="label">Country:</div>
                            <Select
                                options={[{ value: 'RS', label: 'Serbia' }, { value: 'HU', label: 'Hungary' }]}
                                onChange={(e) => this.setCountry(e)}
                                styles={myStyle}
                                width={window.innerWidth > 400 ? '220px' : '270px'}
                                defaultValue={this.state.defaultCountry}
                            />
                        </div>
                        <div className="field">
                            <div className="label">Zip code:</div>
                            <TextField label="" variant="outlined" value={this.state.zip_code} onChange={(e) => this.changeZip(e)} />
                        </div>
                        <div className="field">
                            <div className="label">City (for the temperature):</div>
                            <Select
                                options={this.state.cities}
                                onChange={(e) => this.changeCity(e)}
                                styles={myStyle}
                                width={window.innerWidth > 400 ? '220px' : '270px'}
                                defaultValue={this.state.defaultCity}
                            />
                        </div>
                        <div className="field">
                            <div className="label">City (for the address):</div>
                            <TextField label="" variant="outlined" value={this.state.city} onChange={(e) => this.setState({ city: e.target.value })} />
                        </div>
                        <div className="field">
                            <div className="label">Address:</div>
                            <TextField label="" variant="outlined" value={this.state.address} onChange={(e) => this.changeAddress(e)} />
                        </div>
                        <div className="field">
                            <div className="label">Customer:</div>
                            <Mui.Select
                                labelId="demo-simple-select-label"
                                id="demo-simple-select"
                                value={this.state.user_id}
                                onChange={(e) => this.changeUser(e)}
                                defaultValue={0}
                                disabled={true}
                            >
                                {
                                    this.state.users.map((user) => (
                                        <Mui.MenuItem value={user.id} key={user.id}>{user.name}</Mui.MenuItem>
                                    ))
                                }

                            </Mui.Select>
                        </div>
                        <div className="field">
                            <div className="label">MAC:</div>
                            <TextField id="standard-basic" label="" value={this.state.MAC} variant="outlined" onChange={(e) => this.changeMAC(e)} />
                        </div>
                        <div className="field">
                            <div className="label">Coordinate:</div>
                            <TextField id="standard-basic" label="" value={this.state.coordinate} variant="outlined" onChange={(e) => this.changeCoordinate(e)} />
                        </div>
                        <div className="field">
                            <div className="label">Auto:</div>
                            <Mui.Select
                                value={this.state.auto}
                                onChange={(e) => this.changeAuto(e)}
                                defaultValue={this.state.auto}
                            >
                                <Mui.MenuItem value={0}>MANUAL</Mui.MenuItem>
                                <Mui.MenuItem value={1}>1</Mui.MenuItem>
                                <Mui.MenuItem value={2}>2</Mui.MenuItem>
                                <Mui.MenuItem value={3}>3</Mui.MenuItem>
                                <Mui.MenuItem value={4}>4</Mui.MenuItem>
                                <Mui.MenuItem value={5}>5</Mui.MenuItem>
                                <Mui.MenuItem value={6}>6</Mui.MenuItem>
                                <Mui.MenuItem value={7}>7</Mui.MenuItem>

                            </Mui.Select>
                        </div>
                        <div className="field">
                            <div className="label">Free:</div>
                            <Mui.Select
                                value={this.state.free}
                                onChange={(e) => this.changeFree(e)}
                                defaultValue={this.state.fre}
                            >
                                <Mui.MenuItem value={0}>Yes</Mui.MenuItem>
                                <Mui.MenuItem value={1}>No</Mui.MenuItem>

                            </Mui.Select>
                        </div>
                    </div>

                    <div className="button">
                        <Button variant="contained" color="primary" onClick={() => this.editPump()}>
                            Edit pump
                        </Button>
                    </div>
                </div>
            )
        } else {
            return (
                <div className="modal">
                    <div className="modal-title">
                        <h1>Edit pump</h1>
                        <div className="modal-close" onClick={() => this.props.close()}>x</div>
                    </div>                    <CircularProgress />
                </div>
            )
        }

    }
}

const myStyle = {
    indicatorsContainer: (provided, state) => ({
        width: '30px',
        height: '56px',
        alignItems: 'center',
        display: 'flex',
        padding: '0 5px'

    }),
    control: (_, { selectProps: { width } }) => ({
        width: width,
        height: '56px',
        display: 'flex',
        borderColor: 'rgba(0, 0, 0, 0.23)',
        borderWidth: '1px',
        borderRadius: '4px',
        borderStyle: 'solid'
    }),
    option: (provided, state) => ({
        ...provided,
        borderBottom: '1px dotted pink',
        padding: 20,
        background: state.isFocused ? '#d9d9d9' : '#ffffff',
        color: '#000000'
    }),
}