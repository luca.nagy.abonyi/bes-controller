import React, { Component } from 'react'
import PumpService from '../../services/pumpService'
import Button from '@mui/material/Button';

export default class ResetPump extends Component {
    constructor(props) {
        super(props)
        this.state = {
            pump_id: this.props.pump_id
        }
    }

    async resetPump() {
        await PumpService.reset(this.state.pump_id).then(res => {
            this.props.close()
            this.props.reset()
        })
    }

    render() {
        return (
            <div className="modal">
                <div className="modal-title">
                    <h1>Restart pump</h1>
                    <div className="modal-close" onClick={() => this.props.close()}>x</div>
                </div>
                <div className="action-buttons">
                    <Button variant='contained' onClick={() => this.props.close()}>Cancel</Button>
                    <Button variant='contained' onClick={() => this.resetPump()}>Restart</Button>
                </div>
            </div>
        )
    }
}
