import React, { Component } from 'react';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import PumpService from '../../services/pumpService';
import Select from 'react-select'
import hungarianCities from '../../assets/json/hungarianCities.json';
import serbianCities from '../../assets/json/serbianCities.json';

export default class AddPump extends Component {

    constructor(props) {
        super(props)
        this.state = {
            address: '',
            zip_code: '',
            city: '',
            city_code: '',
            MAC: '',
            coordinate: '',
            country: '',
            cities: serbianCities
        }
    }

    changeZip(e) {
        this.setState({
            zip_code: e.target.value
        })
    }

    changeCity(e) {
        this.setState({
            city: e.label,
            city_code: e.value
        })
    }

    changeAddress(e) {
        this.setState({
            address: e.target.value
        })
    }

    changeMAC(e) {
        this.setState({
            MAC: e.target.value
        })
    }

    changeCoordinate(e) {
        this.setState({
            coordinate: e.target.value
        })
    }

    async addPump() {
        let pump ={
            zip_code: this.state.zip_code,
            city: this.state.city,
            city_code: this.state.city_code,
            address: this.state.address,
            coordinate: this.state.coordinate,
            country: this.state.country,
            MAC: this.state.MAC
        }
       await PumpService.create(pump).then(res => {
            this.props.close()
            this.props.add()
        }) 
    }

    setCountry(e) {
        if (e.value === 'RS') {
            this.setState({
                cities: serbianCities,
                country: 'RS'
            })
        } else {
            this.setState({
                cities: hungarianCities,
                country: 'HU'
            })
        }
    }

    render() {
        return (
            <div className="modal">
                <div className="modal-title">
                    <h1>Add new pump</h1>
                    <div className="modal-close" onClick={() => this.props.close()}>x</div>
                </div>
                <div className="field">
                    <div className="label">Country:</div>
                    <Select
                        options={[{ value: 'RS', label: 'Serbia' }, { value: 'HU', label: 'Hungary' }]}
                        onChange={(e) => this.setCountry(e)}
                        styles={myStyle}
                        width={window.innerWidth > 400 ? '220px' : '270px'}
                    />
                </div>
                <div className="field">
                    <div className="label">Zip code:</div>
                    <TextField id="standard-basic" label="" variant="outlined" onChange={(e) => this.changeZip(e)} />
                </div>
                <div className="field">
                    <div className="label">City (for the temperature):</div>
                    <Select
                        options={this.state.cities}
                        onChange={(e) => this.changeCity(e)}
                        styles={myStyle}
                        width={window.innerWidth > 400 ? '220px' : '270px'}
                    />
                </div>
                <div className="field">
                    <div className="label">City (for the address):</div>
                    <TextField id="standard-basic" label="" variant="outlined" onChange={(e) => this.setState({ city: e.target.value })} />
                </div>
                <div className="field">
                    <div className="label">Address:</div>
                    <TextField id="standard-basic" label="" variant="outlined" onChange={(e) => this.changeAddress(e)} />
                </div>
                <div className="field">
                    <div className="label">MAC:</div>
                    <TextField id="standard-basic" label="" variant="outlined" onChange={(e) => this.changeMAC(e)} />
                </div>
                <div className="field">
                    <div className="label">Coordinate:</div>
                    <TextField id="standard-basic" label="" variant="outlined" onChange={(e) => this.changeCoordinate(e)} />
                </div>
                <div className="button">
                    <Button variant="contained" color="primary" onClick={() => this.addPump()}>
                        Add new pump
                    </Button>
                </div>
            </div>
        )
    }
}
const myStyle = {
    indicatorsContainer: (provided, state) => ({
        width: '30px',
        height: '56px',
        alignItems: 'center',
        display: 'flex',
        padding: '0 5px'

    }),
    control: (_, { selectProps: { width } }) => ({
        width: width,
        height: '56px',
        display: 'flex',
        borderColor: 'rgba(0, 0, 0, 0.23)',
        borderWidth: '1px',
        borderRadius: '4px',
        borderStyle: 'solid'
    }),
    option: (provided, state) => ({
        ...provided,
        borderBottom: '1px dotted pink',
        padding: 20,
        background: state.isFocused ? '#d9d9d9' : '#ffffff',
        color: '#000000'
    }),
}