import React, { Component } from 'react'
import Select from 'react-select'
import hungarianCities from '../../assets/json/hungarianCities.json';
import serbianCities from '../../assets/json/serbianCities.json';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import CompanyService from '../../services/companyService';
import UserService from '../../services/userService';
import * as Mui from '@mui/material/';
import companyService from '../../services/companyService';

export default class EditCompany extends Component {
    constructor(props) {
        super(props)
        console.log(props)
        this.state = {
            name: '',
            phone: '',
            email: '',
            zip_code: '',
            city: '',
            address: '',
            country: '',
            cities: serbianCities,
            city_code: 0,
            user_id: 0,
            users: [],
            defaultCity: {},
            defaultCountry: {}
        }
    }

    componentDidMount() {
        this.getData()
    }

    async getData() {
        await CompanyService.getCompany(this.props.company_id).then(res => {
            let defaultCountry = {
                label: res.data.address.country === 'HU' ? 'Hungary' : 'Serbia',
                value: res.data.address.country === 'HU' ? 'HU' : 'RS',
            }

            let defaultCity = {
                label: res.data.address.city ? res.data.address.city : '',
                value: res.data.address.city_code ? res.data.address.city_code : '',

            }

            this.setState({
                company: res.data,
                name: res.data.name,
                phone: res.data.phone,
                email: res.data.email,
                zip_code: res.data.address.zip_code,
                city: res.data.address.city,
                address: res.data.address.address,
                country: res.data.address.country,
                cities: serbianCities,
                city_code: res.data.address.city_code,
                user_id: res.data.admin.id,
                defaultCountry: defaultCountry,
                defaultCity: defaultCity
            })
        })

        await UserService.getAdmins().then(res => {
            this.setState({
                users: res.data,
            })
        })
    }

    setCountry(e) {
        if (e.value === 'RS') {
            this.setState({
                cities: serbianCities,
                country: 'RS'
            })
        } else {
            this.setState({
                cities: hungarianCities,
                country: 'HU'
            })
        }
    }

    async editCompany() {
        await companyService.update(this.props.company_id, this.state).then(res => {
            this.props.close()
        })
    }

    render() {
        if (this.state.company) {
            return (
                <div className="modal">
                    <div className="modal-title">
                        <h1>Edit company</h1>
                        <div className="modal-close" onClick={() => this.props.close()}>x</div>
                    </div>
                    <div className="fields-container">
                        <div className="field">
                            <div className="label">Name:</div>
                            <TextField value={this.state.name}
                                id="standard-basic" variant="outlined"
                                onChange={(e) => this.setState({ name: e.target.value })} />
                        </div>
                        <div className="field">
                            <div className="label">Phone:</div>
                            <TextField value={this.state.phone} id="standard-basic" variant="outlined" onChange={(e) => this.setState({ phone: e.target.value })} />
                        </div>
                        <div className="field">
                            <div className="label">Email:</div>
                            <TextField id="standard-basic" value={this.state.email} variant="outlined" onChange={(e) => this.setState({ email: e.target.value })} />
                        </div>
                        <div className="field">
                            <div className="label">Admin:</div>
                            <Mui.Select
                                labelId="demo-simple-select-label"
                                id="demo-simple-select"
                                value={this.state.user_id}
                                onChange={(e) => {
                                    this.setState({
                                        user_id: e.target.value
                                    })
                                }}
                                defaultValue={0}
                            >
                                {
                                    this.state.users.map((user) => (
                                        <Mui.MenuItem value={user.id} key={user.id}>{user.name}</Mui.MenuItem>
                                    ))
                                }
                            </Mui.Select>
                        </div>
                        <div className="field">
                            <div className="label">Country:</div>
                            <Select
                                options={[{ value: 'RS', label: 'Serbia' }, { value: 'HU', label: 'Hungary' }]}
                                onChange={(e) => this.setCountry(e)}
                                styles={myStyle}
                                width={window.innerWidth > 400 ? '220px' : '270px'}
                                defaultValue={this.state.defaultCountry}
                            />
                        </div>
                        <div className="field">
                            <div className="label">Zip code:</div>
                            <TextField id="standard-basic" value={this.state.zip_code} variant="outlined" onChange={(e) => this.setState({ zip_code: e.target.value })} />
                        </div>
                        <div className="field">
                            <div className="label">City:</div>
                            <Select
                                options={this.state.cities}
                                onChange={(e) => {
                                    this.setState({
                                        city: e.label,
                                        city_code: e.value
                                    })
                                }}
                                styles={myStyle}
                                width={window.innerWidth > 400 ? '220px' : '270px'}
                                defaultValue={this.state.defaultCity}

                            />
                        </div>
                        <div className="field">
                            <div className="label">Address:</div>
                            <TextField id="standard-basic" value={this.state.address} variant="outlined" onChange={(e) => this.setState({ address: e.target.value })} />
                        </div>
                    </div>
                    <div className="button">
                        <Button variant="contained" color="primary" onClick={() => this.editCompany()}>
                            Edit company
                        </Button>
                    </div>
                </div >
            )
        } else {
            return (
                <div className="modal">
                    <div className="modal-title">
                        <h1>Edit company</h1>
                        <div className="modal-close" onClick={() => this.props.close()}>x</div>
                    </div>
                    <Mui.CircularProgress />
                </div>
            )
        }

    }
}
const myStyle = {
    indicatorsContainer: (provided, state) => ({
        width: '30px',
        height: '56px',
        alignItems: 'center',
        display: 'flex',
        padding: '0 5px'

    }),
    control: (_, { selectProps: { width } }) => ({
        width: width,
        height: '56px',
        display: 'flex',
        borderColor: 'rgba(0, 0, 0, 0.23)',
        borderWidth: '1px',
        borderRadius: '4px',
        borderStyle: 'solid'
    }),
    option: (provided, state) => ({
        ...provided,
        borderBottom: '1px dotted pink',
        padding: 20,
        background: state.isFocused ? '#d9d9d9' : '#ffffff',
        color: '#000000'
    }),
}