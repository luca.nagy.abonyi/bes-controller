import React, { Component } from 'react'
import UserService from '../../services/userService'
import Button from '@mui/material/Button';

export default class DeleteCustomer extends Component {
    constructor(props) {
        super(props)
        console.log(props)
        this.state = {
            user_id: this.props.pump_id
        }
    }

    async deleteUser() {
        await UserService.delete(this.state.user_id).then(res => {
            console.log(res.data)
            this.props.close()
        })
    }

    render() {
        return (
            <div className="modal">
                <div className="modal-title">
                    <h1>Delete customer</h1>
                    <div className="modal-close" onClick={() => this.props.close()}>x</div>
                </div>
                <div className="action-buttons">
                    <Button variant='contained' onClick={() => this.props.close()}>Cancel</Button>
                    <Button variant='contained' onClick={() => this.deleteUser()}>Delete</Button>
                </div>
            </div>
        )
    }
}
