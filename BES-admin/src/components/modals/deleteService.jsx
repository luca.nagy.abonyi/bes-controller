import React, { Component } from 'react'
import Button from '@mui/material/Button';
import ServiceService from '../../services/serviceService';

export default class DeleteService extends Component {
    constructor(props) {
        super(props)
        this.state = {
            service_id: this.props.service_id
        }
    }

    async deleteService() {
        await ServiceService.delete(this.state.service_id).then(res => {
            this.props.close()
        })
    }

    render() {
        return (
            <div className="modal">
                <div className="modal-title">
                    <h1>Delete service</h1>
                    <div className="modal-close" onClick={() => this.props.close()}>x</div>
                </div>
                <div className="action-buttons">
                    <Button variant='contained' onClick={() => this.props.close()}>Cancel</Button>
                    <Button variant='contained' onClick={() => this.deleteService()}>Delete</Button>
                </div>
            </div>
        )
    }
}
