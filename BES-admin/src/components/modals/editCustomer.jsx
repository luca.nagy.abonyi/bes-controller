import React, { Component } from 'react';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import UserService from '../../services/userService';
import PumpService from '../../services/pumpService';
import CircularProgress from '@mui/material/CircularProgress';
import hungarianCities from '../../assets/json/hungarianCities.json';
import serbianCities from '../../assets/json/serbianCities.json';
import * as Mui from '@mui/material/';
import CompanyService from '../../services/companyService';

export default class EditCustomer extends Component {

    constructor(props) {
        super(props)
        this.state = {
            name: '',
            phone: '',
            email: '',
            pump_id: 0,
            pump: {},
            zip_code: '',
            city: '',
            address: '',
            password: '123456',
            password_confirmation: '123456',
            user_id: this.props.user_id,
            user: props.user,
            role_id: 0,
            roles: [],
            country: '',
            cities: serbianCities,
            defaultCountry: {},
            defaultCity: {},
            company_id: 0,
            companies: []
        }
    }

    componentDidMount() {
        this.getData()
    }

    async getData() {
        await PumpService.getPumps().then(res => {
            this.setState({
                pumps: res.data
            })
        })

        await CompanyService.getCompanies().then(res => {
            this.setState({
                companies: res.data
            })
        })

        await UserService.getUser(this.props.user_id).then(res => {
            let defaultCountry = {
                label: res.data.user.address.country === 'HU' ? 'Hungary' : 'Serbia',
                value: res.data.user.address.country === 'HU' ? 'HU' : 'RS',
            }

            let defaultCity = {
                label: res.data.user.address.city ? res.data.user.address.city : '',
                value: res.data.user.address.city ? parseInt(res.data.user.address.city_code) : '',
            }

            this.setState({
                name: res.data.user.name,
                phone: res.data.user.phone,
                email: res.data.user.email,
                pump_id: res.data.user.pump_id,
                zip_code: res.data.user.address ? res.data.user.address.zip_code : '',
                city: res.data.user.address ? res.data.user.address.city : '',
                address: res.data.user.address ? res.data.user.address.address : '',
                role_id: res.data.user.role_id,
                country: res.data.address ? res.data.address.country : '',
                defaultCountry: defaultCountry,
                defaultCity: defaultCity,
                company_id: res.data.user.company_id
            })
        })

        await UserService.getRoles().then(res => {
            this.setState({
                roles: res.data
            })
        })
    }

    changeZip(e) {
        this.setState({
            zip_code: e.target.value
        })
    }

    changeCity(e) {
        this.setState({
            city: e.label,
            city_code: e.value
        })
    }

    changeAddress(e) {
        this.setState({
            address: e.target.value
        })
    }

    changeName(e) {
        this.setState({
            name: e.target.value
        })
    }

    changePhone(e) {
        this.setState({
            phone: e.target.value
        })
    }

    changeEmail(e) {
        this.setState({
            email: e.target.value
        })
    }

    changePump(e) {
        this.state.pumps.forEach(p => {
            if (p.id === e.target.value) {
                let defaultCountry = {
                    value: p.address === 'HU' ? 'HU' : 'RS',
                    label: p.address === 'HU' ? 'Hungary' : 'Serbia',
                }

                let defaultCity = {
                    value: p.address ? p.address.city_code : '',
                    label: p.address ? p.address.city : '',
                }

                this.setState({
                    pump: p,
                    address: p.address ? p.address.address : '',
                    zip_code: p.address ? p.address.zip_code : '',
                    city: p.address ? p.address.city : '',
                    pump_id: p.id,
                    country: p.address ? p.address.country : '',
                    defaultCountry: defaultCountry,
                    defaultCity: defaultCity
                })
            }
        })
    }

    async editUser() {
        let customer = {
            name: this.state.name,
            phone: this.state.phone,
            email: this.state.email,
            company_id: this.state.company_id,
            
        }
        await UserService.update(this.props.user_id, customer).then(res => {
            this.props.close()
        })
    }

    async resetPassword() {
        await UserService.resetPassword(this.props.user_id).then(res => {
            this.props.close()
        })
    }

    setCountry(e) {
        if (e.value === 'RS') {
            this.setState({
                cities: serbianCities,
            })
        } else {
            this.setState({
                cities: hungarianCities,
            })
        }
    }


    render() {
        if (this.state.pumps !== undefined) {
            return (
                <div className="modal">
                    <div className="modal-title">
                        <h1>Edit {this.state.user ? 'user' : 'customer'}</h1>
                        <div className="modal-close" onClick={() => this.props.close()}>x</div>
                    </div>                    <div className="fields-container">
                        <div className="field">
                            <div className="label">Pump:</div>
                            <Mui.Select
                                labelId="demo-simple-select-label"
                                id="demo-simple-select"
                                value={this.state.pump_id}
                                onChange={(e) => this.changePump(e)}
                                defaultValue={0}
                            >
                                {
                                    this.state.pumps.map((pump) => (
                                        <Mui.MenuItem value={pump.id} key={pump.id}>{pump.id}</Mui.MenuItem>
                                    ))
                                }

                            </Mui.Select>
                        </div>
                        <div className="field">
                            <div className="label">Country:</div>
                            <TextField label="" variant="outlined" value={this.state.defaultCountry.label} disabled={true} />

                        </div>
                        <div className="field">
                            <div className="label">Zip code:</div>
                            <TextField label="" variant="outlined" value={this.state.zip_code} disabled={true} />
                        </div>
                        <div className="field">
                            <div className="label">City:</div>
                            <TextField label="" variant="outlined" value={this.state.defaultCity.label} disabled={true} />

                        </div>
                        <div className="field">
                            <div className="label">Address:</div>
                            <TextField label="" variant="outlined" value={this.state.address} disabled={true} />
                        </div>
                        <div className="field">
                            <div className="label">Name:</div>
                            <TextField label="" variant="outlined" value={this.state.name} onChange={(e) => this.changeName(e)} />
                        </div>
                        <div className="field">
                            <div className="label">Phone:</div>
                            <TextField label="" variant="outlined" value={this.state.phone} onChange={(e) => this.changePhone(e)} />
                        </div>
                        <div className="field">
                            <div className="label">Email:</div>
                            <TextField label="" variant="outlined" value={this.state.email} onChange={(e) => this.changeEmail(e)} />
                        </div>
                        {
                            this.state.user ?
                                <div className="field">
                                    <div className="label">Role:</div>
                                    <Mui.Select
                                        labelId="demo-simple-select-label"
                                        id="demo-simple-select"
                                        value={this.state.role_id}
                                        onChange={(e) => {
                                            this.setState({
                                                role_id: e.target.value
                                            })
                                        }}
                                        defaultValue={0}
                                    >
                                        {
                                            this.state.roles.map((role) => (
                                                <Mui.MenuItem value={role.id} key={role.id}>{role.name}</Mui.MenuItem>
                                            ))
                                        }
                                    </Mui.Select>
                                </div>
                                : <div></div>
                        }
                         <div className="field">
                                <div className="label">Company:</div>
                                <Mui.Select
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"
                                    value={this.state.company_id}
                                    onChange={(e) => {
                                        this.setState({
                                            company_id: e.target.value
                                        })
                                    }}
                                    defaultValue={0}
                                >
                                    <Mui.MenuItem value={0} key={0}>-</Mui.MenuItem>

                                    {
                                        this.state.companies.map((company) => (
                                            <Mui.MenuItem value={company.id} key={company.id}>{company.name}</Mui.MenuItem>
                                        ))
                                    }
                                </Mui.Select>
                            </div>
                    </div>

                    <div className="buttons">
                        <Button variant="outlined" color="secondary" onClick={() => this.resetPassword()}>
                            Reset password
                        </Button>

                        <Button variant="contained" color="primary" onClick={() => this.editUser()}>
                            Edit {this.state.user ? 'user' : 'customer'}
                        </Button>

                    </div>
                </div>
            )
        } else {
            return (
                <div className="modal">
                    <div className="modal-title">
                        <h1>Edit {this.state.user ? 'user' : 'customer'}</h1>
                        <div className="modal-close" onClick={() => this.props.close()}>x</div>
                    </div>
                    <CircularProgress />
                </div>
            )
        }
    }
}
