import React, { Component } from 'react'
import CompanyService from '../../services/companyService'
import Button from '@mui/material/Button';

export default class DeleteCompany extends Component {
    constructor(props) {
        super(props)
        console.log(props)
        this.state = {
            company_id: this.props.pump_id
        }
    }

    async deleteCompany() {
        await CompanyService.delete(this.state.company_id).then(res => {
            this.props.close()
        })
    }

    render() {
        return (
            <div className="modal">
                <div className="modal-title">
                    <h1>Delete company</h1>
                    <div className="modal-close" onClick={() => this.props.close()}>x</div>
                </div>
                <div className="action-buttons">
                    <Button variant='contained' onClick={() => this.props.close()}>Cancel</Button>
                    <Button variant='contained' onClick={() => this.deleteCompany()}>Delete</Button>
                </div>
            </div>
        )
    }
}
