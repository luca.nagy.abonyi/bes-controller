import React, { Component } from 'react'
import Select from 'react-select'
import hungarianCities from '../../assets/json/hungarianCities.json';
import serbianCities from '../../assets/json/serbianCities.json';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import UserService from '../../services/userService';
import * as Mui from '@mui/material/';
import CompanyService from '../../services/companyService';

export default class AddCompany extends Component {
    constructor(props) {
        super(props)

        this.state = {
            name: '',
            phone: '',
            email: '',
            zip_code: '',
            city: '',
            address: '',
            country: '',
            cities: serbianCities,
            city_code: 0,
            user_id: 0,
            users: []
        }
    }

    componentDidMount() {
        this.getData()
    }

    async getData() {
        await UserService.getAdmins().then(res => {
            console.log(res.data)
            this.setState({
                users: res.data
            })
        })
    }

    setCountry(e) {
        if (e.value === 'RS') {
            this.setState({
                cities: serbianCities,
                country: 'RS'
            })
        } else {
            this.setState({
                cities: hungarianCities,
                country: 'HU'
            })
        }
    }

    async addCompany() {
        await CompanyService.create(this.state).then(res => {
            this.props.close()
        })
    }

    render() {
        return (
            <div className="modal">
                <div className="modal-title">
                    <h1>Add new company </h1>
                    <div className="modal-close" onClick={() => this.props.close()}>x</div>
                </div>
                <div className="fields-container">
                    <div className="field">
                        <div className="label">Name:</div>
                        <TextField id="standard-basic" label="" variant="outlined" onChange={(e) => this.setState({ name: e.target.value })} />
                    </div>
                    <div className="field">
                        <div className="label">Phone:</div>
                        <TextField id="standard-basic" label="" variant="outlined" onChange={(e) => this.setState({ phone: e.target.value })} />
                    </div>
                    <div className="field">
                        <div className="label">Email:</div>
                        <TextField id="standard-basic" label="" variant="outlined" onChange={(e) => this.setState({ email: e.target.value })} />
                    </div>
                    <div className="field">
                        <div className="label">Admin:</div>
                        <Mui.Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={this.state.user_id}
                            onChange={(e) => {
                                this.setState({
                                    user_id: e.target.value
                                })
                            }}
                            defaultValue={0}
                        >
                            {
                                this.state.users.map((user) => (
                                    <Mui.MenuItem value={user.id} key={user.id}>{user.name}</Mui.MenuItem>
                                ))
                            }
                        </Mui.Select>
                    </div>
                    <div className="field">
                        <div className="label">Country:</div>
                        <Select
                            options={[{ value: 'RS', label: 'Serbia' }, { value: 'HU', label: 'Hungary' }]}
                            onChange={(e) => this.setCountry(e)}
                            styles={myStyle}
                            width='220px'
                        />
                    </div>
                    <div className="field">
                        <div className="label">Zip code:</div>
                        <TextField id="standard-basic" label="" variant="outlined" onChange={(e) => this.setState({ zip_code: e.target.value })} />
                    </div>
                    <div className="field">
                        <div className="label">City:</div>
                        <Select
                            options={this.state.cities}
                            onChange={(e) => {
                                this.setState({
                                    city: e.label,
                                    city_code: e.value
                                })
                            }}
                            styles={myStyle}
                            width='220px'
                        />
                    </div>
                    <div className="field">
                        <div className="label">Address:</div>
                        <TextField id="standard-basic" label="" variant="outlined" onChange={(e) => this.setState({ address: e.target.value })} />
                    </div>
                </div>
                <div className="button">
                    <Button variant="contained" color="primary" onClick={() => this.addCompany()}>
                        Add new company
                    </Button>
                </div>
            </div >
        )
    }
}
const myStyle = {
    indicatorsContainer: (provided, state) => ({
        width: '30px',
        height: '56px',
        alignItems: 'center',
        display: 'flex',
        padding: '0 5px'

    }),
    control: (_, { selectProps: { width } }) => ({
        width: width,
        height: '56px',
        display: 'flex',
        borderColor: 'rgba(0, 0, 0, 0.23)',
        borderWidth: '1px',
        borderRadius: '4px',
        borderStyle: 'solid'
    }),
    option: (provided, state) => ({
        ...provided,
        borderBottom: '1px dotted pink',
        padding: 20,
        background: state.isFocused ? '#d9d9d9' : '#ffffff',
        color: '#000000'
    }),
}