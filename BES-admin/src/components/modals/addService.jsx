import React, { Component } from 'react'
import PumpService from '../../services/pumpService';
import UserService from '../../services/userService';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import CircularProgress from '@mui/material/CircularProgress';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import ServiceService from '../../services/serviceService';

export default class AddService extends Component {
    constructor(props) {
        super(props)
        this.state = {
            pump_id: 0,
            user_id: 0,
            comment: '',
            workers: [],
            pumps: []
        }
    }

    componentDidMount() {
        this.getData()
    }


    async getData() {
        await UserService.getWorkers().then(res => {
            console.log(res.data)
            this.setState({
                workers: res.data
            })
        })

        await PumpService.getPumps().then(res => {
            console.log(res.data)
            this.setState({
                pumps: res.data
            })
        })
    }

    async addService() {
        let service = {
            pump_id: this.state.pump_id,
            user_id: this.state.user_id,
            comment: this.state.comment
        }

        await ServiceService.create(service).then(res => {
            this.props.close()
        })
    }


    render() {
        if (this.state.pumps.length > 0) {
            return (
                <div className='modal'>
                    <div className="modal-title">
                        <h1>Add new service</h1>
                        <div className="modal-close" onClick={() => this.props.close()}>x</div>
                    </div>
                    <div className="field">
                        <div className="label">Pump:</div>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={this.state.pump_id}
                            onChange={(e) => {
                                this.setState({
                                    pump_id: e.target.value
                                })
                            }}
                            defaultValue={0}
                        >
                            <MenuItem value={0} >-</MenuItem>
                            {
                                this.state.pumps.map((pump) => (
                                    <MenuItem value={pump.id} key={pump.id}>{pump.id}</MenuItem>
                                ))
                            }
                        </Select>
                    </div>
                    <div className="field">
                        <div className="label">Worker:</div>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={0}
                            onChange={(e) => {
                                this.setState({
                                    user_id: e.target.value
                                })
                            }}
                            defaultValue={0}
                        >
                            <MenuItem value={0} >-</MenuItem>

                            {
                                this.state.workers.map((worker) => (
                                    <MenuItem value={worker.id} key={worker.id}>{worker.name}</MenuItem>
                                ))
                            }
                        </Select>
                    </div>
                    <div className="field">
                        <div className="label">Comment:</div>
                        <TextField label="" variant="outlined"
                            value={this.state.comment}
                            multiline
                            maxRows={4}
                            rows={3}
                            onChange={(e) => {
                                this.setState({
                                    comment: e.target.value
                                })
                            }} />
                    </div>
                    <div className="button">
                        <Button variant="contained" color="primary" onClick={() => this.addService()}>
                            Add new service
                        </Button>
                    </div>
                </div>
            )
        } else {
            return (
                <div className="modal">
                    <div className="modal-title">
                        <h1>Add new service</h1>
                        <div className="modal-close" onClick={() => this.props.close()}>x</div>
                    </div>                    <CircularProgress />
                </div>
            )
        }
    }
}