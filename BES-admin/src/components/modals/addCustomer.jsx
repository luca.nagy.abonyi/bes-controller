import React, { Component } from 'react';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import UserService from '../../services/userService';
import PumpService from '../../services/pumpService';
import CompanyService from '../../services/companyService';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import CircularProgress from '@mui/material/CircularProgress';

export default class AddCustomer extends Component {

    constructor(props) {
        super(props)
        let role
        if (this.props.user) {
            role = 1
        } else {
            role = 4
        }
        this.state = {
            name: '',
            phone: '',
            email: '',
            pump_id: 0,
            pump: {},
            zip_code: '',
            city: '',
            address: '',
            password: '123456',
            password_confirmation: '123456',
            role_id: role,
            user: this.props.user,
            roles: [],
            company_id: 0,
            companies: []
        }
    }

    componentDidMount() {
        this.getData()
    }

    async getData() {
        await PumpService.getPumps().then(res => {
            this.setState({
                pumps: res.data
            })
        })

        await CompanyService.getCompanies().then(res => {
            this.setState({
                companies: res.data
            })
        })

        if (this.state.user) {
            await UserService.getRoles().then(res => {
                this.setState({
                    roles: res.data
                })
            })
        }
    }

    changeZip(e) {
        this.setState({
            zip_code: e.target.value
        })
    }

    changeCity(e) {
        this.setState({
            city: e.target.value
        })
    }

    changeAddress(e) {
        this.setState({
            address: e.target.value
        })
    }

    changeName(e) {
        this.setState({
            name: e.target.value
        })
    }

    changePhone(e) {
        this.setState({
            phone: e.target.value
        })
    }

    changeEmail(e) {
        this.setState({
            email: e.target.value
        })
    }

    changePump(e) {
        this.state.pumps.forEach(p => {
            if (p.id === e.target.value) {
                this.setState({
                    pump: p,
                    address: p.address ? p.address.address : '',
                    zip_code: p.address ? p.address.zip_code : '',
                    city: p.address ? p.address.city : '',
                    pump_id: p.id
                })
            }
        })
    }

    async addUser() {
        if (this.state.company_id === 0) {
            this.setState({
                company_id: null
            })
        }
        await UserService.create(this.state).then(res => {
            this.props.close()
        }).catch(error => {
            this.setState({
                phone_error: error.response.data.phone,
                name_error: error.response.data.name,
            })
        })
    }

    render() {
        if (this.state.pumps !== undefined) {
            return (
                <div className="modal">
                    <div className="modal-title">
                        <h1>Add new {this.state.user ? 'user' : 'customer'}</h1>
                        <div className="modal-close" onClick={() => this.props.close()}>x</div>
                    </div>
                    <div className="fields-container">
                        <div className="field">
                            <div className="label">Pump:</div>
                            <Select
                                labelId="demo-simple-select-label"
                                id="demo-simple-select"
                                value={this.state.pump_id}
                                onChange={(e) => this.changePump(e)}
                                defaultValue={0}
                            >
                                <MenuItem value={0} key={0}>-</MenuItem>
                                {
                                    this.state.pumps.map((pump) => (
                                        <MenuItem value={pump.id} key={pump.id}>{pump.id}</MenuItem>
                                    ))
                                }
                            </Select>
                        </div>
                        <div className="field">
                            <div className="label">Zip code:</div>
                            <TextField label="" variant="outlined" value={this.state.zip_code} disabled={true} />
                        </div>
                        <div className="field">
                            <div className="label">City name:</div>
                            <TextField label="" variant="outlined" value={this.state.city} disabled={true} />
                        </div>
                        <div className="field">
                            <div className="label">Address:</div>
                            <TextField label="" variant="outlined" value={this.state.address} disabled={true} />
                        </div>
                        <div className="field">
                            <div className="label">Name*:</div>
                            <TextField label="" variant="outlined" value={this.state.name} onChange={(e) => this.changeName(e)} 
                            error={this.state.name_error ? true : false}
                            helperText={this.state.name_error ? this.state.name_error : ''}/>
                        </div>
                        <div className="field">
                            <div className="label">Phone*:</div>
                            <TextField label="" variant="outlined" value={this.state.phone} onChange={(e) => this.changePhone(e)} 
                            error={this.state.phone_error ? true : false}
                            helperText={this.state.phone_error ? this.state.phone_error : ''}/>
                        </div>
                        <div className="field">
                            <div className="label">Email:</div>
                            <TextField label="" variant="outlined" value={this.state.email} onChange={(e) => this.changeEmail(e)} />
                        </div>
                        {
                            this.state.user ?
                                <div className="field">
                                    <div className="label">Role:</div>
                                    <Select
                                        labelId="demo-simple-select-label"
                                        id="demo-simple-select"
                                        value={this.state.role_id}
                                        onChange={(e) => {
                                            this.setState({
                                                role_id: e.target.value
                                            })
                                        }}
                                        defaultValue={0}
                                    >
                                        <MenuItem value={0} key={0}>-</MenuItem>

                                        {
                                            this.state.roles.map((role) => (
                                                <MenuItem value={role.id} key={role.id}>{role.name}</MenuItem>
                                            ))
                                        }
                                    </Select>
                                </div>
                                : <div></div>
                        }
                        <div>
                            <div className="field">
                                <div className="label">Company:</div>
                                <Select
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"
                                    value={this.state.company_id}
                                    onChange={(e) => {
                                        this.setState({
                                            company_id: e.target.value
                                        })
                                    }}
                                    defaultValue={0}
                                >
                                    <MenuItem value={0} key={0}>-</MenuItem>

                                    {
                                        this.state.companies.map((company) => (
                                            <MenuItem value={company.id} key={company.id}>{company.name}</MenuItem>
                                        ))
                                    }
                                </Select>
                            </div>
                        </div>
                    </div>
                    <div className="button">
                        <Button variant="contained" color="primary" onClick={() => this.addUser()}>
                            Add new {this.state.user ? 'user' : 'customer'}
                        </Button>
                    </div>
                </div>
            )
        } else {
            return (
                <div className="modal">
                    <div className="modal-title">
                        <h1>Add new {this.state.user ? 'user' : 'customer'}</h1>
                        <div className="modal-close" onClick={() => this.props.close()}>x</div>
                    </div>
                    <CircularProgress />
                </div>
            )
        }
    }
}
