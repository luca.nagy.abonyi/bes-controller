import React, { Component } from 'react'
import { NavLink } from "react-router-dom";
import './components.scss';
import HomeIcon from '@mui/icons-material/Home';
import LogoutIcon from '@mui/icons-material/Logout';
import logo from '../assets/logo2.png';
import AuthService from '../services/authService';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import MenuIcon from '@mui/icons-material/Menu';
import data from '../assets/json/routes.json';
import Icon from '@mui/material/Icon';

export default class Sidenavbar extends Component {

    constructor(props) {
        super(props)
        this.state = {
            user: {},
            role: "",
            open: false,
            routes: data.routes,
            roles: data.roles
        }
    }

    componentDidMount() {
        this.getData()
    }

    async getData() {
        await AuthService.auth(JSON.parse(localStorage.token)).then(res => {
            let routes = []
            let role
            data.roles.forEach(r => {
                if (r.id === res.data.role_id) {
                    role = r
                    console.log(role.name)
                }
            })

            data.routes.forEach(route => {
                if (role.routes.includes(route.id)) {
                    routes.push(route)
                }
            })

            this.setState({
                user: res.data,
                role: role.name,
                routes: routes
            })
        })
    }

    logout() {
        AuthService.logout()
    }


    render() {
        return (
            <div>
                <div className={this.state.open ? "sidenav-container-open" : "sidenav-container"}>
                    {/* <div className="menu-button">
                        <MenuIcon onClick={() => this.setState({ open: !this.state.open })}></MenuIcon>
                    </div> */}
                    <div className="profile-container">
                        <NavLink to='/home'><img src={logo} alt="logo" className="logo" /></NavLink>
                        <NavLink to='/profile' className="profile" activeClassName="active-menu">
                            <AccountCircleIcon />
                            <div>
                                <div className="profile-name">{this.state.user.name}</div>
                                <div className="profile-role">{this.state.role}</div>
                            </div>
                        </NavLink>
                    </div>
                    <div className="menus">
                        <NavLink to='/home' className="menu" activeClassName="active-menu">
                            <div className="menu-icon"><HomeIcon /></div>
                            <div className="menu-text">Home</div>
                        </NavLink>
                        {
                            this.state.routes.map(route =>
                                <NavLink to={route.route} className="menu" activeClassName="active-menu" key={route.id}>
                                    <div className="menu-icon"><Icon>{route.icon}</Icon></div>
                                    <div className="menu-text">{route.name}</div>
                                </NavLink>
                            )
                        }
                        <NavLink to='/login' className="menu" onClick={() => this.logout()}>
                            <div className="menu-icon"><LogoutIcon /></div>
                            <div className="menu-text">Logout</div>
                        </NavLink>

                    </div>
                </div>
            </div >
        )
    }
}
