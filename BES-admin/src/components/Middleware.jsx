import React, { Component } from 'react'
import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';
import { withRouter } from "react-router-dom";

class Middleware extends Component {
    constructor(props) {
        super(props)
        console.log("local " + localStorage.getItem('auth'))
        let auth = localStorage.getItem('token')
        if(auth !== ''){
            this.props.history.push({ pathname: "/home"});
        } else {
            this.props.history.push({ pathname: "/login"});

        }
        


    }
    render() {
        return (
            <div style={{ width: '100vw', height: '100vh', display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                <Box sx={{ display: 'flex' }}>
                    <CircularProgress />
                </Box>
            </div>
        )
    }
}

export default withRouter(Middleware);
