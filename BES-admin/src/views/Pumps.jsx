import React, { Component } from 'react'
import pumpService from '../services/pumpService';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import './styles.scss';
import CheckIcon from '@mui/icons-material/Check';
import ClearIcon from '@mui/icons-material/Clear';
import CreateIcon from '@mui/icons-material/Create';
import DeleteIcon from '@mui/icons-material/Delete';
import TextField from '@mui/material/TextField';
import Skeleton from '@mui/material/Skeleton';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import AddCircleIcon from '@mui/icons-material/AddCircle';
import Modal from '@mui/material/Modal';
import AddPump from '../components/modals/addPump';
import EditPump from '../components/modals/editPump';
import DeletePump from '../components/modals/deletePump';
import Collapse from '@mui/material/Collapse';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';
import Snackbar from '@mui/material/Snackbar';
import RestartAltIcon from '@mui/icons-material/RestartAlt';
import ResetPump from '../components/modals/resetPump';
import PanToolIcon from '@mui/icons-material/PanTool';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import AuthService from '../services/authService';

export default class Pumps extends Component {
    constructor(props) {
        super(props)
        this.state = {
            addOpen: false,
            editOpen: false,
            deleteOpen: false,
            selected: 0,
            isAdmin: false,
            isReseller: false,
            addSnack: false,
            pumps: []
        }
    }

    componentDidMount() {
        this.getData()
    }

    async getData() {
        await pumpService.getPumps().then(res => {
            this.setState({
                pumps: res.data
            })
        })

        await AuthService.auth(JSON.parse(localStorage.token)).then(res => {
            if (res.data.role_id < 3) {
                this.setState({
                    isAdmin: true
                })
            }

            if (res.data.role_id === 3) {
                this.setState({
                    isReseller: true
                })
            }
        })
    }

    openAdd() {
        this.setState({
            addOpen: true
        })
    }

    close() {
        this.getData();
        this.setState({
            addOpen: false,
        })
    }

    restart() {
        this.setState({
            pumps: []
        })
        this.getData();
    }
    render() {
        if (this.state.pumps.length !== 0) {
            return (
                <div className="page-container" id="pumps-container">
                    <div className="table-topbar">
                        <TextField label="Search" variant="filled" />
                        <div className="table-topbar-refresh">
                            <RestartAltIcon sx={{ color: " #840033", cursor: 'pointer' }} onClick={() => this.restart()} />
                            <Button variant="outlined" startIcon={<AddCircleIcon />} color="secondary" onClick={() => this.openAdd()}>
                                Add new pump
                            </Button>
                        </div>
                    </div>
                    <div className="table-container">
                        <TableContainer>
                            <Table sx={{ minWidth: 650 }} size="small" aria-label="a dense table">
                                <TableHead>
                                    <TableRow>
                                        <TableCell align="left"><div className="table-header">ID</div></TableCell>
                                        <TableCell align="left"><div className="table-header">
                                            <Tooltip title="Click on value and edit">
                                                <div className="table-header">Auto
                                                    <CreateIcon fontSize="small" />
                                                </div>
                                            </Tooltip></div></TableCell>
                                        <TableCell align="left"><div className="table-header">Online</div></TableCell>
                                        <TableCell align="left"><div className="table-header">Address</div></TableCell>
                                        <TableCell align="left"><div className="table-header">Temperature</div></TableCell>
                                        <TableCell align="left">
                                            <Tooltip title="Click on value and edit">
                                                <div className="table-header">Set temperature
                                                    <CreateIcon fontSize="small" />
                                                </div>
                                            </Tooltip>
                                        </TableCell>
                                        <TableCell align="left"><div className="table-header">User</div></TableCell>
                                        <TableCell align="left"><div className="table-header">Company</div></TableCell>
                                        <TableCell align="left"><div className="table-header">Actions</div></TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {this.state.pumps.map((row) => (
                                        <Row key={row.id} row={row} close={() => this.close()} admin={this.state.isAdmin} reseller={this.state.isReseller}></Row>
                                    ))}
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </div>
                    <Modal
                        open={this.state.addOpen}
                        onClose={() => this.close()}
                        aria-labelledby="modal-modal-title"
                        aria-describedby="modal-modal-description"
                    >
                        <Box sx={{ ...style, width: window.innerWidth > 400 ? 400 : 280 }}>
                            <AddPump close={() => this.close()} add={() => this.setState({ addSnack: true })} />
                        </Box>
                    </Modal>
                    <Snackbar
                        open={this.state.addSnack}
                        autoHideDuration={6000}
                        onClose={() => this.setState({ addSnack: false })}
                        message="Pump added."
                    />
                </div >
            )
        }
        else {
            return (
                <div className="progress-container">
                    <div className="table-topbar">
                        <TextField label="Search" variant="filled" />
                        <div className="table-topbar-refresh">
                            <RestartAltIcon sx={{ color: " #840033", cursor: 'pointer' }} onClick={() => this.restart()} />
                            <Button variant="outlined" startIcon={<AddCircleIcon />} color="secondary" onClick={() => this.openAdd()}>
                                Add new pump
                            </Button>
                        </div>
                    </div>
                    <Box sx={{ width: '100%' }}>
                        <Skeleton />
                        <Skeleton />
                        <Skeleton animation="wave" />
                        <Skeleton animation={false} />
                    </Box>
                </div>
            )
        }

    }
}

function Row(props) {
    const row = props.row;
    const [open, setOpen] = React.useState(false);

    const [openTemp, setOpenTemp] = React.useState(false);
    const [temp, setTemp] = React.useState(row.set_temp);
    const [tempSnack, setTempSnack] = React.useState(false);

    const [editOpen, setEditOpen] = React.useState(false);
    const [deleteOpen, setDeleteOpen] = React.useState(false);
    const [resetOpen, setResetOpen] = React.useState(false);

    const [auto, setAuto] = React.useState(row.auto);
    const [openAuto, setOpenAuto] = React.useState(false);
    const [autoSnack, setAutoSnack] = React.useState(false);
    const [editSnack, setEditSnack] = React.useState(false);
    const [resetSnack, setResetSnack] = React.useState(false);
    const [deleteSnack, setDeleteSnack] = React.useState(false);


    async function onSetTemp() {
        let pump = {
            temp: temp
        }

        await pumpService.setTemp(row.id, pump).then(res => {
            setOpenTemp(false)
            setTempSnack(true)
            setTemp(temp)
        })
    }

    async function onSetAuto() {
        let pump = row;
        pump.auto = auto;

        await pumpService.auto(pump).then(res => {
            setOpenAuto(false)
            setAutoSnack(true)
            setAuto(auto)
        })
    }

    function handleClose() {
        setEditOpen(false)
        setDeleteOpen(false)
        setResetOpen(false)
        props.close()
    }

    function handleEdit() {
        setEditSnack(true)
    }

    function handleReset() {
        setResetSnack(true)
    }

    function handleDelete() {
        setDeleteSnack(true)
    }

    return (
        <React.Fragment>
            <TableRow sx={{ '& > *': { borderBottom: 'unset' } }} className={row.online === 1 ? 'row-online' : 'row-offline'}
                style={open ? { borderBottom: "none" } : { borderBottom: "1px solid white" }}>
                <TableCell scope="row">
                    {row.id}
                </TableCell>
                <TableCell align="left">

                    <TableCell align="left" className="row-temperature" >
                        <div onClick={() => setOpenAuto(!openAuto)} style={{ cursor: 'pointer', marginRight: 5 }}>
                            {openAuto ? '' :
                                row.auto === 0 ?
                                    <Tooltip title="0-MANUAL">
                                        <PanToolIcon size="small" />
                                    </Tooltip>
                                    :
                                    <div>
                                        {row.auto}
                                    </div>
                            }
                        </div>
                        {
                            openAuto ?
                                <div className="editable-cell-field">
                                    <Select
                                        value={auto}
                                        onChange={(e) => setAuto(e.target.value)}
                                        variant="filled"
                                    >
                                        <MenuItem value={0}>MANUAL</MenuItem>
                                        <MenuItem value={1}>1</MenuItem>
                                        <MenuItem value={2}>2</MenuItem>
                                        <MenuItem value={3}>3</MenuItem>
                                        <MenuItem value={4}>4</MenuItem>
                                        <MenuItem value={5}>5</MenuItem>
                                        <MenuItem value={6}>6</MenuItem>
                                        <MenuItem value={7}>7</MenuItem>

                                    </Select>

                                    <IconButton size='small' onClick={() => onSetAuto()}>
                                        <CheckIcon fontSize="small" />
                                    </IconButton>
                                    <IconButton size='small'
                                        onClick={() => {
                                            setAuto(row.auto);
                                            setOpenAuto(!openAuto)
                                        }}>
                                        <ClearIcon fontSize="small" />
                                    </IconButton>
                                </div>
                                :
                                <div></div>}
                    </TableCell>
                </TableCell>
                <TableCell align="left">
                    {
                        row.online === 0 ?
                            <ClearIcon />
                            :
                            <CheckIcon />
                    }
                </TableCell>
                <TableCell align="left">
                    <div className="address-cell">
                        {
                            row.address ?
                                <div>
                                    <div>{row.address.zip_code}</div>
                                    <div>{row.address.city}</div>
                                    <div>{row.address.address}</div>
                                </div>
                                :
                                <div></div>
                        }

                    </div>
                </TableCell>

                <TableCell align="left">
                    <div>{row.temperature} &#186;C</div>
                </TableCell>

                <TableCell align="left" className="row-temperature" >
                    <div onClick={() => setOpenTemp(!openTemp)} style={{ cursor: 'pointer', marginRight: 5 }}>{openTemp ? '' : temp} </div>
                    {
                        openTemp ?
                            <div className="editable-cell-field">
                                <TextField value={temp} variant="filled" onChange={(e) => setTemp(e.target.value)} />
                                <IconButton size='small' onClick={() => onSetTemp()}>
                                    <CheckIcon fontSize="small" />
                                </IconButton>
                                <IconButton size='small'
                                    onClick={() => {
                                        setTemp(row.set_temp);
                                        setOpenTemp(!openTemp)
                                    }}>
                                    <ClearIcon fontSize="small" />
                                </IconButton>
                            </div>
                            :
                            <div> &#186;C</div>}
                </TableCell>
                <TableCell align="left">
                    {
                        row.user ?
                            <div>
                                <div>{row.user.name}</div>
                            </div>
                            :
                            <div></div>
                    }
                </TableCell>
                <TableCell align="left">
                    {
                        row.company ?
                            <div>
                                <div>{row.company}</div>
                            </div>
                            :
                            <div>-</div>
                    }
                </TableCell>
                <TableCell align="left">
                    <IconButton
                        aria-label="expand row"
                        size="small"
                        onClick={() => setOpen(!open)}
                    >
                        {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                    </IconButton>
                    {
                        props.admin ?
                            <div>
                                <IconButton
                                    aria-label="expand row"
                                    size="small"
                                    onClick={() => setEditOpen(true)}>
                                    <CreateIcon className="icon-button" />
                                </IconButton>
                                <IconButton
                                    aria-label="expand row"
                                    size="small"
                                    onClick={() => setDeleteOpen(true)}>
                                    <DeleteIcon className="icon-button" />
                                </IconButton>
                                <IconButton
                                    aria-label="expand row"
                                    size="small"
                                    onClick={() => setResetOpen(true)}>
                                    <RestartAltIcon className="icon-button" />
                                </IconButton>
                            </div>
                            :
                                <IconButton
                                    aria-label="expand row"
                                    size="small"
                                    onClick={() => setResetOpen(true)}>
                                    <RestartAltIcon className="icon-button" />
                                </IconButton>
                    }
                    {
                        props.reseller ?
                            <IconButton
                                aria-label="expand row"
                                size="small"
                                onClick={() => setEditOpen(true)}>
                                <CreateIcon className="icon-button" />
                            </IconButton>
                            :
                            <div></div>
                    }
                </TableCell>
            </TableRow>
            <TableRow style={open ? { borderBottom: "1px solid white" } : { borderBottom: "none" }} className={row.online === 1 ? 'row-online' : 'row-offline'}>
                <TableCell className={open ? "row-details-open" : "row-details-close"} colSpan={9}>
                    <Collapse in={open} timeout="auto" unmountOnExit>
                        <div className="list-container">
                            <div className="list-row">
                                <div className="title">Last online</div>
                                <div className="data">{row.last_online}</div>
                            </div>
                            <div className="list-row">
                                <div className="title">Free</div>
                                <div className="data">{row.free === 1 ? 'Yes' : 'No'}</div>
                            </div>
                            <div className="list-row">
                                <div className="title">MAC</div>
                                <div className="data">{row.MAC}</div>
                            </div>
                            <div className="list-row">
                                <div className="title">City temperature</div>
                                <div className="data">{row.city_temp} &#186;C</div>
                            </div>
                            <div className="list-row">
                                <div className="title">Status</div>
                                <div className="data">{row.status}</div>
                            </div>
                            <div className="list-row">
                                <div className="title">Last service</div>
                                <div className="data">-</div>
                            </div>
                            <div className="list-row">
                                <div className="title">Last update</div>
                                <div className="data">{row.updated_at}</div>
                            </div>
                        </div>
                    </Collapse>
                </TableCell>
            </TableRow>
            <Snackbar
                open={tempSnack}
                autoHideDuration={6000}
                onClose={() => setTempSnack(false)}
                message="Pump tempreture is set."
            />
            <Snackbar
                open={autoSnack}
                autoHideDuration={6000}
                onClose={() => setAutoSnack(false)}
                message="Pump auto/manual is set."
            />
            <Snackbar
                open={editSnack}
                autoHideDuration={6000}
                onClose={() => setEditSnack(false)}
                message="Pump edited successfully."
            />
            <Snackbar
                open={resetSnack}
                autoHideDuration={6000}
                onClose={() => setResetSnack(false)}
                message="Pump reseted."
            />
            <Snackbar
                open={deleteSnack}
                autoHideDuration={6000}
                onClose={() => setDeleteSnack(false)}
                message="Pump deleted successfully."
            />
            <Modal
                id="edit-pump"
                open={editOpen}
                onClose={() => handleClose()}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={{ ...style, width: window.innerWidth > 400 ? 400 : 280 }}>
                    <EditPump close={() => handleClose()} pump_id={row.id} edit={() => handleEdit()} />
                </Box>
            </Modal>
            <Modal
                open={deleteOpen}
                onClose={() => handleClose()}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={{ ...style, width: window.innerWidth > 400 ? 400 : 280 }}>
                    <DeletePump close={() => handleClose()} pump_id={row.id} delete={() => handleDelete()} />
                </Box>
            </Modal>
            <Modal
                open={resetOpen}
                onClose={() => handleClose()}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={{ ...style, width: window.innerWidth > 400 ? 400 : 280 }}>
                    <ResetPump close={() => handleClose()} pump_id={row.id} reset={() => handleReset()} />
                </Box>
            </Modal>
        </React.Fragment >
    );

}

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: '600px',
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    pt: 2,
    px: 4,
    pb: 3,
};