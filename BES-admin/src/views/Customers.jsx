import React, { Component } from 'react'
import userService from '../services/userService';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import './styles.scss';
import CreateIcon from '@mui/icons-material/Create';
import DeleteIcon from '@mui/icons-material/Delete';
import TextField from '@mui/material/TextField';
import Skeleton from '@mui/material/Skeleton';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import AddCircleIcon from '@mui/icons-material/AddCircle';
import Modal from '@mui/material/Modal';
import AddCustomer from '../components/modals/addCustomer';
import EditCustomer from '../components/modals/editCustomer';
import DeleteCustomer from '../components/modals/deleteCustomer';

export default class Customers extends Component {
    constructor(props) {
        super(props)
        this.state = {
            users: {},
            addOpen: false,
            editOpen: false,
            deleteOpen: false,
            selected: 0
        }
    }

    componentDidMount() {
        this.getData()
    }

    async getData() {
        await userService.getCustomers().then(res => {
            this.setState({
                users: res.data.users
            })
        })
    }

    openAdd() {
        this.setState({
            addOpen: true
        })
    }

    openEdit(id) {
        this.setState({
            editOpen: true,
            selected: id
        })
    }

    openDelete(id) {
        this.setState({
            deleteOpen: true,
            selected: id
        })
    }

    handleClose() {
        this.getData();
        this.setState({
            addOpen: false,
            editOpen: false,
            deleteOpen: false
        })
    }

    render() {
        if (this.state.users.length > 0) {
            return (
                <div className="page-container" >
                    <div className="table-topbar">
                        <TextField id="standard-basic" label="Search" variant="filled" />
                        <Button variant="outlined" startIcon={<AddCircleIcon />} color="secondary" onClick={() => this.openAdd()}>
                            Add new customer
                        </Button>
                    </div>
                    <div className="table-container">
                        <TableContainer>
                            <Table sx={{ minWidth: 650 }} size="small" aria-label="a dense table">
                                <TableHead>
                                    <TableRow>
                                        <TableCell>ID</TableCell>
                                        <TableCell align="left">Pump id</TableCell>
                                        <TableCell align="left">Name</TableCell>
                                        <TableCell align="left">Phone</TableCell>
                                        <TableCell align="left">Address</TableCell>
                                        <TableCell align="left">Email</TableCell>
                                        <TableCell align="left">Company</TableCell>
                                        <TableCell align="left">Actions</TableCell>

                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {this.state.users.map((row) => (
                                        <TableRow
                                            key={row.id}
                                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                        >
                                            <TableCell scope="row">
                                                {row.id}
                                            </TableCell>
                                            <TableCell align="left">
                                                {row.pump_id}
                                            </TableCell>
                                            <TableCell align="left">
                                                {row.name}
                                            </TableCell>
                                            <TableCell align="left">
                                                {row.phone}
                                            </TableCell>
                                            <TableCell align="left">
                                                <div className="address-cell">
                                                    {
                                                        row.address ?
                                                            <div>
                                                                <div>{row.address.zip_code}</div>
                                                                <div>{row.address.city}</div>
                                                                <div>{row.address.address}</div>

                                                            </div>
                                                            :
                                                            <div></div>
                                                    }

                                                </div>
                                            </TableCell>
                                            <TableCell align="left">
                                                {row.email}
                                            </TableCell>
                                            <TableCell align="left">
                                                {row.company}
                                            </TableCell>
                                            <TableCell align="left">
                                                <CreateIcon onClick={() => this.openEdit(row.id)} />
                                                <DeleteIcon onClick={() => this.openDelete(row.id)} />
                                            </TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </div>
                    <Modal
                        open={this.state.addOpen}
                        onClose={() => this.handleClose()}
                        aria-labelledby="modal-modal-title"
                        aria-describedby="modal-modal-description"
                    >
                        <Box sx={{ ...style, width: window.innerWidth > 400 ? 400 : 280}}>
                            <AddCustomer close={() => this.handleClose()} user={false}/>
                        </Box>
                    </Modal>
                    <Modal
                        open={this.state.editOpen}
                        onClose={() => this.handleClose()}
                        aria-labelledby="modal-modal-title"
                        aria-describedby="modal-modal-description"
                    >
                        <Box sx={{ ...style, width: window.innerWidth > 400 ? 400 : 280}}>
                            <EditCustomer close={() => this.handleClose()} user_id={this.state.selected} user={false}/>
                        </Box>
                    </Modal>
                    <Modal
                        open={this.state.deleteOpen}
                        onClose={() => this.handleClose()}
                        aria-labelledby="modal-modal-title"
                        aria-describedby="modal-modal-description"
                    >
                        <Box sx={{ ...style, width: window.innerWidth > 400 ? 400 : 280}}>
                            <DeleteCustomer close={() => this.handleClose()} user_id={this.state.selected} />
                        </Box>
                    </Modal>
                </div >
            )
        }
        else {
            return (
                <div className="progress-container">
                    <div className="table-topbar">
                        <TextField id="standard-basic" label="Search" variant="filled" />
                    </div>
                    <Box sx={{ width: '100%' }}>
                        <Skeleton />
                        <Skeleton />
                        <Skeleton animation="wave" />
                        <Skeleton animation={false} />
                    </Box>
                </div>
            )
        }

    }
}

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: '600px',
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    pt: 2,
    px: 4,
    pb: 3,
};