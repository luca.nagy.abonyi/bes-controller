import React, { Component } from 'react'
import "./styles.scss"
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Select from 'react-select'
import hungarianCities from '../assets/json/hungarianCities.json';
import serbianCities from '../assets/json/serbianCities.json';
import UserService from '../services/userService';
import AuthService from '../services/authService';
import Skeleton from '@mui/material/Skeleton';
import Box from '@mui/material/Box';
import CompanyService from '../services/companyService';

export default class Profile extends Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            phone: '',
            email: '',
            zip_code: '',
            city: '',
            address: '',
            role_id: 0,
            roles: [],
            country: '',
            cities: serbianCities,
            defaultCountry: {},
            defaultCity: {},
            password: '',
            password_confirmation: '',
            company: {}
        }
    }

    componentDidMount() {
        this.getData()
    }

    async getData() {
        await AuthService.auth(JSON.parse(localStorage.token)).then(res => {
            let defaultCountry = {}
            let defaultCity = {}
            
            if (res.data.company) {
                defaultCountry = {
                    label: res.data.company.address === 'HU' ? 'Hungary' : 'Serbia',
                    value: res.data.company.address === 'HU' ? 'HU' : 'RS',
                }

                defaultCity = {
                    label: res.data.company.address ? res.data.company.address.city : '',
                    value: res.data.company.address ? parseInt(res.data.company.address.city_code) : '',
                }
            }
            let admin = false
            if (res.data.role_id < 3) {
                admin = true
            }

            this.setState({
                name: res.data.name,
                phone: res.data.phone,
                email: res.data.email,
                company_zip_code: res.data.company.address ? res.data.company.address.zip_code : '',
                company_city: res.data.company.address ? res.data.company.address.city : '',
                company_address: res.data.company.address ? res.data.company.address.address : '',
                role_id: res.data.role_id,
                company_country: res.data.company.address ? res.data.company.address.country : '',
                company_city_code: res.data.company.address ? res.data.company.address.city_code : '',
                defaultCountry: defaultCountry,
                defaultCity: defaultCity,
                user: res.data,
                company_name: res.data.company.name,
                company_email: res.data.company.email,
                company_phone: res.data.company.phone,
                isAdmin: admin
            })
        })

    }

    async save() {
        let user = {
            name: this.state.name,
            email: this.state.email,
            phone: this.state.phone,
        }

        if (this.state.password !== '') {
            user.password = this.state.password
            user.password_confirmation = this.state.password_confirmation
        }

        await UserService.update(this.state.user.id, user).then(res => {
        })
    }

    setCountry(e) {
        if (e.value === 'RS') {
            this.setState({
                cities: serbianCities,
                country: 'RS'
            })
        } else {
            this.setState({
                cities: hungarianCities,
                country: 'HU'
            })
        }
    }

    async saveCompany() {
        let company = {
            email: this.state.company_email,
            phone: this.state.company_phone,
            name: this.state.company_name,
            country: this.state.company_country,
            address: this.state.company_address,
            zip_code: this.state.company_zip_code,
            city_code: this.state.company_city_code,
            city: this.state.company_city
        }

        await CompanyService.update(this.state.user.company_id, company)
    }

    render() {
        if (this.state.user !== undefined) {
            return (
                <div className='profile-page-container'>
                    <div>
                        <h1>Profile settings</h1>
                        <div className="field">
                            <div className="label">Name:</div>
                            <TextField label="" variant="outlined"
                                value={this.state.name}
                                onChange={(e) => {
                                    this.setState({
                                        name: e.target.value
                                    })
                                }} />
                        </div>
                        <div className="field">
                            <div className="label">Phone:</div>
                            <TextField label="" variant="outlined"
                                value={this.state.phone}
                                onChange={(e) => {
                                    this.setState({
                                        phone: e.target.value
                                    })
                                }} />
                        </div>
                        <div className="field">
                            <div className="label">Email:</div>
                            <TextField label="" variant="outlined"
                                value={this.state.email}
                                onChange={(e) => {
                                    this.setState({
                                        email: e.target.value
                                    })
                                }} />
                        </div>
                        <div className="field">
                            <div className="label">Password:</div>
                            <TextField
                                type='password'
                                variant="outlined"
                                value={this.state.password}
                                onChange={(e) => {
                                    this.setState({
                                        password: e.target.value,
                                        password_confirmation: e.target.value
                                    })
                                }} />
                        </div>
                        <div className="button">
                            <Button variant='contained' color='primary' onClick={() => this.save()}>
                                Save
                            </Button>
                        </div>
                    </div>
                    <div className="profile-company-container">
                        {
                            this.state.user.company_id !== null ?
                                <div>
                                    <h1>Company settings</h1>

                                    <div className="company-fields">
                                        <div>
                                            <div className="field">
                                                <div className="label">Company name:</div>
                                                <TextField label="" variant="outlined"
                                                    value={this.state.company_name}
                                                    disabled={!this.state.isAdmin}
                                                    onChange={(e) => {
                                                        this.setState({
                                                            company_name: e.target.value
                                                        })
                                                    }} />
                                            </div>
                                            <div className="field">
                                                <div className="label">Company country:</div>
                                                <Select
                                                    options={[{ value: 'RS', label: 'Serbia' }, { value: 'HU', label: 'Hungary' }]}
                                                    onChange={(e) => this.setState({
                                                        company_country: e.value
                                                    })}
                                                    styles={myStyle}
                                                    width={window.innerWidth > 400 ? '210px' : '270px'}
                                                    defaultValue={this.state.defaultCountry}
                                                    isDisabled={!this.state.isAdmin}

                                                />
                                            </div>
                                            <div className="field">
                                                <div className="label">Company city:</div>
                                                <Select
                                                    options={this.state.cities}
                                                    onChange={(e) => this.setState({
                                                        company_city: e.value,
                                                        company_city_code: e.label
                                                    })}
                                                    styles={myStyle}
                                                    width={window.innerWidth > 400 ? '210px' : '270px'}
                                                    defaultValue={this.state.defaultCity}
                                                    isDisabled={!this.state.isAdmin}
                                                />
                                            </div>

                                            <div className="field">
                                                <div className="label">Company zip code:</div>
                                                <TextField label="" variant="outlined"
                                                    value={this.state.company_zip_code}
                                                    disabled={!this.state.isAdmin}
                                                    onChange={(e) => {
                                                        this.setState({
                                                            company_zip_code: e.target.value
                                                        })
                                                    }} />
                                            </div>
                                        </div>
                                        <div>
                                            <div className="field">
                                                <div className="label">Company address:</div>
                                                <TextField label="" variant="outlined"
                                                    value={this.state.company_address}
                                                    disabled={!this.state.isAdmin}
                                                    onChange={(e) => {
                                                        this.setState({
                                                            company_address: e.target.value
                                                        })
                                                    }} />
                                            </div>
                                            <div className="field">
                                                <div className="label">Company phone:</div>
                                                <TextField label="" variant="outlined"
                                                    value={this.state.company_phone}
                                                    disabled={!this.state.isAdmin}
                                                    onChange={(e) => {
                                                        this.setState({
                                                            company_phone: e.target.value
                                                        })
                                                    }} />
                                            </div>
                                            <div className="field">
                                                <div className="label">Company email:</div>
                                                <TextField label="" variant="outlined"
                                                    value={this.state.company_email}
                                                    disabled={!this.state.isAdmin}
                                                    onChange={(e) => {
                                                        this.setState({
                                                            company_email: e.target.value
                                                        })
                                                    }} />
                                            </div>

                                        </div>

                                    </div>
                                    <div className="button">
                                        <Button variant='contained' color='primary' onClick={() => this.saveCompany()}>
                                            Save
                                        </Button>
                                    </div>
                                </div>
                                :
                                <div></div>
                        }
                    </div>

                </div>
            )
        } else {
            return (
                <div>
                    <div className='profile-page-container'>
                        <h1>Profile settings</h1>
                    </div>
                    <div className='profile-page-container'>
                        <Box sx={{ width: '25%' }} style={{ marginRight: '100px' }}>
                            <Skeleton />
                            <Skeleton />
                            <Skeleton animation="wave" />
                            <Skeleton animation={false} />
                        </Box>
                    </div>

                </div>
            )
        }

    }
}
const myStyle = {
    indicatorsContainer: (provided, state) => ({
        width: '30px',
        height: '56px',
        alignItems: 'center',
        display: 'flex',
        padding: '0 5px'

    }),
    control: (_, { selectProps: { width } }) => ({
        width: width,
        height: '56px',
        display: 'flex',
        borderColor: 'rgba(0, 0, 0, 0.23)',
        borderWidth: '1px',
        borderRadius: '4px',
        borderStyle: 'solid'
    }),
    option: (provided, state) => ({
        ...provided,
        borderBottom: '1px dotted pink',
        padding: 20,
        background: state.isFocused ? '#d9d9d9' : '#ffffff',
        color: '#000000'
    }),
}