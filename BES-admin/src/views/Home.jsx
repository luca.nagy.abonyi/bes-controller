import React, { Component } from 'react'
import './styles.scss';
import AuthService from '../services/authService';
import cities from '../assets/json/cities.json';
import { Doughnut } from 'react-chartjs-2';
import chartService from '../services/chartService';
import Skeleton from '@mui/material/Skeleton';
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
    ArcElement
} from 'chart.js';
import { Line } from 'react-chartjs-2';

ChartJS.register(
    ArcElement,
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend);

export const options = {
    responsive: true,
    plugins: {
        legend: {
            position: 'top',
        },
        title: {
            display: true,
            text: 'Pumps temperature',
        },
    },
};

export default class Home extends Component {
    constructor(props) {
        super(props)
        this.state = {
            user: {},
            citiestemps: cities
        }
    }

    componentDidMount() {
        this.getData()
    }

    async getData() {
        await AuthService.auth(JSON.parse(localStorage.token)).then(res => {
            this.setState({
                user: res.data
            })
        })

        await chartService.getChartsData().then(res => {

            this.setState({
                online_pumps: {
                    labels: ['Online', 'Offline'],
                    datasets: [
                        {
                            label: '# of pumps',
                            data: res.data.online_pumps,
                            backgroundColor: [
                                'rgba(154, 188, 167, 0.2)',
                                'rgba(132, 0, 51, 0.2)',
                            ],
                            borderColor: [
                                'rgba(154, 188, 167, 1)',
                                'rgba(132, 0, 51, 1)',

                            ],
                            borderWidth: 1,
                        },
                    ]
                },
                pumps_temp: {
                    labels: res.data.pump_temp_labels,
                    datasets: [
                        {
                            label: 'Pump temperature',
                            data: res.data.pumps_temp,
                            borderColor: 'rgba(132, 0, 51, 1)',
                            backgroundColor: 'rgba(132, 0, 51, 0.5)',
                        },
                        {
                            label: 'Pump set temperature',
                            data: res.data.pumps_set_temp,
                            borderColor: 'rgba(154, 188, 167, 1)',
                            backgroundColor: 'rgba(154, 188, 167, 0.5)',
                        },
                    ]
                }
            })
        })
    }
    render() {
        return (
            <div className='home-container'>
                <h1>Welcome {this.state.user.name}!</h1>
                <div className="charts-container">
                    <div className="data-container">
                        <div className="home-wheater">
                            <div id="icon">
                                <img id="wicon"
                                    src={'http://openweathermap.org/img/w/' + this.state.citiestemps[0].wheater_data.weather[0].icon + '.png'}
                                    alt="Weather icon" />
                            </div>
                            <div>
                                <div className="temperature">{this.state.citiestemps[0].temp} &#186;C</div>
                                <div className='city-name'>{this.state.citiestemps[0].name}</div>
                            </div>
                        </div>
                        <div className="cities-wheater">
                            {
                                this.state.citiestemps.map((row) => (
                                    <div className="city-row">
                                        <img id="wicon"
                                            src={'http://openweathermap.org/img/w/' + row.wheater_data.weather[0].icon + '.png'}
                                            alt="Weather icon"
                                            width='25px' />
                                        <div className='city-row-name'>{row.name}</div>
                                        <div className='city-row-temp'>{row.temp} &#186;C</div>
                                    </div>
                                ))
                            }
                        </div>
                    </div>
                    <div className="online-pumps">
                        {
                            this.state.online_pumps !== undefined ?
                                <Doughnut data={this.state.online_pumps} />
                                :
                                <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                                    <Skeleton variant="circular" width={200} height={200} />
                                </div>

                        }
                    </div>
                    <div className="pumps-temps">
                        {
                            this.state.pumps_temp !== undefined ?
                                <Line options={options} data={this.state.pumps_temp} />
                                :
                                <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                                    <Skeleton variant="rectangular" width={500} height={200} />
                                </div>

                        }
                    </div>
                </div>

            </div >
        )
    }
}
