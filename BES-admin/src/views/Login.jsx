import React, { Component } from 'react'
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import AuthService from '../services/authService';
import './styles.scss';
import logo from '../assets/logo2.png';
import store from '../index';
import { auth } from '../actions/actions';
import Backdrop from '@mui/material/Backdrop';
import CircularProgress from '@mui/material/CircularProgress';

export default class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            password: '',
            open: false
        }


    }

    componentDidMount() {
        document.addEventListener("keydown", event => this.handleKeyPress(event));
    }

    changeEmail(e) {
        this.setState({
            email: e.target.value
        })
    }

    changePassword(e) {
        this.setState({
            password: e.target.value
        })
    }

    handleKeyPress(event) {
        if (event.key === 'Enter') {
            this.login()
        }
    }

    async login() {
        this.setState({
            open: true
        })
        await AuthService.login(this.state).then(res => {
            if (res.data.token) {
                store.dispatch(auth(true))
                localStorage.setItem("token", JSON.stringify(res.data.token));
                localStorage.setItem("auth", true);
                this.props.history.push({ pathname: '/home' });
            }
        }).catch(error => {
            this.setState({
                email_error: error.response.data.email,
                password_error: error.response.data.password,
                unauth: error.response.data.message,
                open: false
            })
        })
    }

    render() {
        return (
            <div className="login-container">
                <img src={logo} alt="logo" />
                <h3>Heat Pump Controller Admin Panel</h3>
                <div className="field">
                    <TextField label="Email"
                        variant="outlined"
                        onChange={(e) => this.changeEmail(e)}
                        value={this.state.email}
                        onKeyPress={(e) => this.handleKeyPress(e)}
                        error={this.state.email_error ? true : false}
                        helperText={this.state.email_error ? this.state.email_error : ''} />
                </div>
                <div className="field">
                    <TextField label="Password"
                        variant="outlined"
                        type="password"
                        onChange={(e) => this.changePassword(e)}
                        value={this.state.password}
                        onKeyPress={(e) => this.handleKeyPress(e)}
                        error={this.state.password_error ? true : false}
                        helperText={this.state.password_error ? this.state.password_error : ''} />
                    <div className='error'>
                        {this.state.unauth}
                    </div>
                </div>

                <Button variant="contained" color="primary" onClick={() => this.login()}>
                    Login
                </Button>
                <Backdrop
                    sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
                    open={this.state.open}
                >
                    <CircularProgress color="inherit" />
                </Backdrop>
            </div>
        )
    }
}
