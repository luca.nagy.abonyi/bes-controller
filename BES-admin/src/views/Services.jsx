import React, { Component } from 'react'
import './styles.scss';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import DeleteIcon from '@mui/icons-material/Delete';
import Skeleton from '@mui/material/Skeleton';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import AddCircleIcon from '@mui/icons-material/AddCircle';
import Modal from '@mui/material/Modal';
import ServiceService from '../services/serviceService';
import AddService from '../components/modals/addService'
import DeleteService from '../components/modals/deleteService';

export default class Services extends Component {
    constructor(props) {
        super(props)
        this.state = {
            openAdd: false,
            openEdit: false,
            openDelete: false,
            service_id: ''
        }
    }

    componentDidMount() {
        this.getData()
    }

    async getData() {
        await ServiceService.getServices().then(res => {
            this.setState({
                services: res.data
            })
        })
    }

    openAdd() {
        this.setState({
            openAdd: true
        })
    }

    openDelete(id) {
        this.setState({
            openDelete: true,
            service_id: id
        })
    }

    handleClose() {
        this.setState({
            openAdd: false,
            openEdit: false,
            openDelete: false
        })
        this.getData()
    }

    render() {
        if (this.state.services) {
            return (
                <div className="page-container">
                    <div style={{
                        float: 'right', marginBottom: '20px',
                        display: 'flex',
                        justifyContent: 'space-between',
                        height: '55px'
                    }}>
                        <Button variant="outlined" startIcon={<AddCircleIcon />} color="secondary" onClick={() => this.openAdd()}>
                            Add new service
                        </Button>
                    </div>
                    <div className="table-container">
                        <TableContainer>
                            <Table sx={{ minWidth: 650 }} size="small" aria-label="a dense table">
                                <TableHead>
                                    <TableRow>
                                        <TableCell>#</TableCell>
                                        <TableCell align="left">Pump id</TableCell>
                                        <TableCell align="left">Worker</TableCell>
                                        <TableCell align="left">Comment</TableCell>
                                        <TableCell align="left">Actions</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {this.state.services.map((row, index) => (
                                        <TableRow
                                            key={row.id}
                                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                        >
                                            <TableCell scope="row">
                                                {index + 1}
                                            </TableCell>
                                            <TableCell align="left">
                                                {row.pump_id}
                                            </TableCell>
                                            <TableCell align="left">
                                                {row.user_name}
                                            </TableCell>
                                            <TableCell align="left">
                                                {row.comment}
                                            </TableCell>
                                            <TableCell align="left">
                                                <DeleteIcon onClick={() => this.openDelete(row.id)} style={{ cursor: 'pointer' }} />
                                            </TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </div>
                    <Modal
                        open={this.state.openAdd}
                        onClose={() => this.handleClose()}
                        aria-labelledby="modal-modal-title"
                        aria-describedby="modal-modal-description"
                    >
                        <Box sx={{ ...style, width: window.innerWidth > 400 ? 400 : 280}}>
                            <AddService close={() => this.handleClose()} />
                        </Box>
                    </Modal>
                    <Modal
                        open={this.state.openDelete}
                        onClose={() => this.handleClose()}
                        aria-labelledby="modal-modal-title"
                        aria-describedby="modal-modal-description"
                    >
                        <Box sx={{ ...style, width: window.innerWidth > 400 ? 400 : 280}}>
                            <DeleteService close={() => this.handleClose()} service_id={this.state.service_id} />
                        </Box>
                    </Modal>
                </div>
            )
        } else {
            return (
                <div className="progress-container">
                    <div className="table-topbar">
                    </div>
                    <Box sx={{ width: '100%' }}>
                        <Skeleton />
                        <Skeleton />
                        <Skeleton animation="wave" />
                        <Skeleton animation={false} />
                    </Box>
                </div>
            )
        }

    }
}


const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: '600px',
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    pt: 2,
    px: 4,
    pb: 3,
};