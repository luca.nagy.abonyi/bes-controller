import axios from 'axios';

class ServiceService {
  async getServices () {
    try {
      return axios.get (process.env.REACT_APP_PUBLIC_URL + 'services', {
        headers: {Authorization: `Bearer ${JSON.parse (localStorage.token)}`},
      });
    } catch (error) {
      console.log (error);
    }
  }

  async getService (id) {
    try {
      return axios.get (process.env.REACT_APP_PUBLIC_URL + 'services/' + id, {
        headers: {Authorization: `Bearer ${JSON.parse (localStorage.token)}`},
      });
    } catch (error) {
      console.log (error);
    }
  }

  async create (service) {
    try {
      return axios.post (
        process.env.REACT_APP_PUBLIC_URL + 'services',
        service,
        {
          headers: {Authorization: `Bearer ${JSON.parse (localStorage.token)}`},
        }
      );
    } catch (error) {
      console.log (error);
    }
  }

  async update (id, service) {
    try {
      return axios.put (
        process.env.REACT_APP_PUBLIC_URL + 'services/' + id,
        service,
        {
          headers: {Authorization: `Bearer ${JSON.parse (localStorage.token)}`},
        }
      );
    } catch (error) {
      console.log (error);
    }
  }

  async delete (id) {
    console.log (id);
    try {
      return axios.delete (
        process.env.REACT_APP_PUBLIC_URL + 'services/' + id,
        {
          headers: {Authorization: `Bearer ${JSON.parse (localStorage.token)}`},
        }
      );
    } catch (error) {
      console.log (error);
    }
  }
}
export default new ServiceService ();
