import axios from 'axios';

class ChartService {
  async getCitiesTemp () {
    try {
      return axios.get (process.env.REACT_APP_PUBLIC_URL + 'city_temps', {
        headers: {Authorization: `Bearer ${JSON.parse (localStorage.token)}`},
      });
    } catch (error) {
      console.log (error);
    }
  }

  async getChartsData () {
    try {
      return axios.get (process.env.REACT_APP_PUBLIC_URL + 'charts-data', {
        headers: {Authorization: `Bearer ${JSON.parse (localStorage.token)}`},
      });
    } catch (error) {
      console.log (error);
    }
  }
}

export default new ChartService ();
