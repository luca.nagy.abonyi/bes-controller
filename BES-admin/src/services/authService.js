import axios from 'axios';
import store from '../index';
import {auth} from '../actions/actions';

class AuthService {
  async auth (token) {
    try {
      return axios.get (process.env.REACT_APP_PUBLIC_URL+'profile', 
      {
        headers: {Authorization: `Bearer ${token}`},
      });
    } catch (error) {
      console.log (error);
    }
  }

  async login (json) {
    try {
      return axios.post (
        process.env.REACT_APP_PUBLIC_URL + 'admin-login',
        json
      );
    } catch (error) {
      console.log (error);
    }
  }

  async logout () {
    store.dispatch (auth (false));
    window.location.reload (false);
    localStorage.setItem ('token', '');
    localStorage.setItem ('auth', false);
  }
}

export default new AuthService ();
