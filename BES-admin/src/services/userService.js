import axios from 'axios';

class UserService {
  async getUsers () {
    try {
      return axios.get (process.env.REACT_APP_PUBLIC_URL + 'users', {
        headers: {Authorization: `Bearer ${JSON.parse (localStorage.token)}`},
      });
    } catch (error) {
      console.log (error);
    }
  }

  async getUser (id) {
    try {
      return axios.get (process.env.REACT_APP_PUBLIC_URL + 'users/' + id, {
        headers: {Authorization: `Bearer ${JSON.parse (localStorage.token)}`},
      });
    } catch (error) {
      console.log (error);
    }
  }

  async create (user) {
    try {
      return axios.post (process.env.REACT_APP_PUBLIC_URL + 'register', user, {
        headers: {Authorization: `Bearer ${JSON.parse (localStorage.token)}`},
      });
    } catch (error) {
      console.log (error);
    }
  }

  async update (id, user) {
    try {
      return axios.put (
        process.env.REACT_APP_PUBLIC_URL + 'users/' + id,
        user,
        {
          headers: {Authorization: `Bearer ${JSON.parse (localStorage.token)}`},
        }
      );
    } catch (error) {
      console.log (error);
    }
  }

  async delete (id) {
    console.log (id);
    try {
      return axios.delete (process.env.REACT_APP_PUBLIC_URL + 'users/' + id, {
        headers: {Authorization: `Bearer ${JSON.parse (localStorage.token)}`},
      });
    } catch (error) {
      console.log (error);
    }
  }

  async getCustomers () {
    try {
      return axios.get (process.env.REACT_APP_PUBLIC_URL + 'customers', {
        headers: {Authorization: `Bearer ${JSON.parse (localStorage.token)}`},
      });
    } catch (error) {
      console.log (error);
    }
  }

  async getWorkers () {
    try {
      return axios.get (process.env.REACT_APP_PUBLIC_URL + 'workers', {
        headers: {Authorization: `Bearer ${JSON.parse (localStorage.token)}`},
      });
    } catch (error) {
      console.log (error);
    }
  }

  async resetPassword (id) {
    try {
      return axios.put (
        process.env.REACT_APP_PUBLIC_URL + 'reset-password/' + id,
        {
          headers: {Authorization: `Bearer ${JSON.parse (localStorage.token)}`},
        }
      );
    } catch (error) {
      console.log (error);
    }
  }

  async getAllUsers () {
    try {
      return axios.get (process.env.REACT_APP_PUBLIC_URL + 'all-users', {
        headers: {Authorization: `Bearer ${JSON.parse (localStorage.token)}`},
      });
    } catch (error) {
      console.log (error);
    }
  }

  async getAdmins () {
    try {
      return axios.get (process.env.REACT_APP_PUBLIC_URL + 'admins', {
        headers: {Authorization: `Bearer ${JSON.parse (localStorage.token)}`},
      });
    } catch (error) {
      console.log (error);
    }
  }

  async getRoles () {
    try {
      return axios.get (process.env.REACT_APP_PUBLIC_URL + 'roles', {
        headers: {Authorization: `Bearer ${JSON.parse (localStorage.token)}`},
      });
    } catch (error) {
      console.log (error);
    }
  }
}

export default new UserService ();
