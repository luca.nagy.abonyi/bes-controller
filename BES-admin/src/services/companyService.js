import axios from 'axios';

class CompanyService {
  async getCompanies () {
    try {
      return axios.get (process.env.REACT_APP_PUBLIC_URL + 'companies', {
        headers: {Authorization: `Bearer ${JSON.parse (localStorage.token)}`},
      });
    } catch (error) {
      console.log (error);
    }
  }

  async getCompany (id) {
    try {
      return axios.get (process.env.REACT_APP_PUBLIC_URL + 'companies/' + id, {
        headers: {Authorization: `Bearer ${JSON.parse (localStorage.token)}`},
      });
    } catch (error) {
      console.log (error);
    }
  }

  async create (company) {
    try {
      return axios.post (
        process.env.REACT_APP_PUBLIC_URL + 'companies',
        company,
        {
          headers: {Authorization: `Bearer ${JSON.parse (localStorage.token)}`},
        }
      );
    } catch (error) {
      console.log (error);
    }
  }

  async update (id, company) {
    try {
      return axios.put (
        process.env.REACT_APP_PUBLIC_URL + 'companies/' + id,
        company,
        {
          headers: {Authorization: `Bearer ${JSON.parse (localStorage.token)}`},
        }
      );
    } catch (error) {
      console.log (error);
    }
  }

  async delete (id) {
    console.log (id);
    try {
      return axios.delete (
        process.env.REACT_APP_PUBLIC_URL + 'companies/' + id,
        {
          headers: {Authorization: `Bearer ${JSON.parse (localStorage.token)}`},
        }
      );
    } catch (error) {
      console.log (error);
    }
  }
}
export default new CompanyService ();
