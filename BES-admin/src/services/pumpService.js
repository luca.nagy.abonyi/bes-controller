import axios from 'axios';

class PumpService {
  async getPumps () {
    try {
      return axios.get (process.env.REACT_APP_PUBLIC_URL + 'pumps',
      {
        headers: {Authorization: `Bearer ${JSON.parse(localStorage.token)}`},
      });
    } catch (error) {
      console.log (error);
    }
  }

  async getPump (id) {
    try {
      return axios.get (process.env.REACT_APP_PUBLIC_URL + 'pumps/' + id);
    } catch (error) {
      console.log (error);
    }
  }

  async create (pump) {
    try {
      return axios.post (process.env.REACT_APP_PUBLIC_URL + 'pumps/', pump);
    } catch (error) {
      console.log (error);
    }
  }

  async update (id, pump) {
    console.log(pump)
    try {
      return axios.put (process.env.REACT_APP_PUBLIC_URL + 'pumps/' + id, pump);
    } catch (error) {
      console.log (error);
    }
  }

  async delete (id) {
    console.log (id);
    try {
      return axios.delete (process.env.REACT_APP_PUBLIC_URL + 'pumps/' + id);
    } catch (error) {
      console.log (error);
    }
  }

  async getAddress (id) {
    try {
      return axios.get (process.env.REACT_APP_PUBLIC_URL + 'addresses/' + id, {
        headers: {Authorization: `Bearer ${JSON.parse(localStorage.token)}`},
      });
    } catch (error) {
      console.log (error);
    }
  }

  async setTemp (id, pump) {
    try {
      return axios.put (
        process.env.REACT_APP_PUBLIC_URL + 'commands/setTemp/' + id,
        pump,
        {
          headers: {Authorization: `Bearer ${JSON.parse(localStorage.token)}`},
        }
      );
    } catch (error) {
      console.log (error);
    }
  }

  async reset (id) {
    try {
      return axios.put (
        process.env.REACT_APP_PUBLIC_URL + 'commands/reset/' + id,
        {
          headers: {Authorization: `Bearer ${JSON.parse(localStorage.token)}`},
        }
      );
    } catch (error) {
      console.log (error);
    }
  }

  async auto (pump) {
    try {
      return axios.put (
        process.env.REACT_APP_PUBLIC_URL + 'pumps/auto',pump,
        {
          headers: {Authorization: `Bearer ${JSON.parse(localStorage.token)}`},
        }
      );
    } catch (error) {
      console.log (error);
    }
  }
}

export default new PumpService ();
/*, {
    headers: {Authorization: `Bearer ${token}`},
}
, {
  headers: {Authorization: `Bearer ${token}`},
}
, {
  headers: {Authorization: `Bearer ${token}`},
}*/
